// ------------
// Reverse2.cpp
// ------------

// http://en.cppreference.com/w/cpp/algorithm/reverse

#include <algorithm> // equal, swap
#include <cassert>   // assert
#include <iostream>  // cout, endl
#include <list>      // list

using namespace std;

#define TEST1
#define TEST2
#define TEST3
#define TEST4
#define TEST5

template <typename BI>
void my_reverse (BI b, BI e) {
    while ((b != e) && (b != --e)) {
        swap(*b, *e);
        ++b;}}

#ifdef TEST1
void test1 () {
    int a[] = {2, 3, 4};
    my_reverse(a, a);
    assert(equal(a, a + 3, begin({2, 3, 4})));}
#endif

#ifdef TEST2
void test2 () {
    int a[] = {2, 3, 4};
    my_reverse(a, a + 1);
    assert(equal(a, a + 3, begin({2, 3, 4})));}
#endif

#ifdef TEST3
void test3 () {
    list<int>           x = {2, 3, 4};
    list<int>::iterator b = begin(x);
    list<int>::iterator e = begin(x);
    ++++e;
    my_reverse(b, e);
    assert(equal(begin(x), end(x), begin({3, 2, 4})));}
#endif

#ifdef TEST4
void test4 () {
    list<int> x = {2, 3, 4};
    my_reverse(begin(x), end(x));
    assert(equal(begin(x), end(x), begin({4, 3, 2})));}
#endif

#ifdef TEST5
void test5 () {
    list<int> x = {2, 3, 4, 5};
    my_reverse(begin(x), end(x));
    assert(equal(begin(x), end(x), begin({5, 4, 3, 2})));}
#endif

int main () {
    cout << "Reverse.cpp" << endl;
    test1();
    test2();
    test3();
    test4();
    test5();
    /*
    int n;
    cin >> n;
    switch (n) {
        #ifdef TEST1
        case 1:
            test1();
            break;
        #endif

        #ifdef TEST2
        case 2:
            test2();
            break;
        #endif

        #ifdef TEST3
        case 3:
            test3();
            break;
        #endif

        #ifdef TEST4
        case 4:
            test4();
            break;
        #endif

        #ifdef TEST5
        case 5:
            test5();
            break;
        #endif

        default:
            assert(false);}
    */
    cout << "Done." << endl;
    return 0;}
