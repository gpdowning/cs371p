// -------------
// Allocator.cpp
// -------------

// http://en.cppreference.com/w/cpp/memory/allocator

#include <algorithm> // equal
#include <array>     // array
#include <cassert>   // assert
#include <iostream>  // cout, endl
#include <memory>    // allocator, destroy, uninitialized_copy, uninitialized_fill
#include <new>       // new

using namespace std;

template <typename T>
void my_construct (T* p, const T& v) {
    new (p) T(v);}

template <typename T>
void my_destroy (T* p) {
    p->~T();}

using array_t = array<size_t, 5>;

class A {
    friend bool operator == (const A& lhs, const A& rhs) {
        return lhs._v == rhs._v;}

    friend bool operator < (const A& lhs, const A& rhs) {
        return lhs._v < rhs._v;}

    public:
        static array_t c;

        static void reset () {
            c = array_t({0, 0, 0, 0, 0});}

    private:
        int _v;

    public:
        A () :                         // default constructor
            _v (0) {
            ++c[0];}

        A (int v) :                    // int constructor
            _v (v) {
            ++c[1];}

        A (const A& rhs) :             // copy constructor
            _v (rhs._v) {
            ++c[2];}

        A& operator = (const A& rhs) { // copy assignment
            _v = rhs._v;
            ++c[3];
            return *this;}

        ~A () {                        // destructor
            ++c[4];}};

array_t A::c;

void test1 () {
    A::reset();
    allocator<A> al;
    assert(A::c == array_t({0, 0, 0, 0, 0}));

    const size_t s = 1;

    A::reset();
    A* a = al.allocate(s);
    assert(A::c == array_t({0, 0, 0, 0, 0}));

    A::reset();
    const A v = 2;
    assert(A::c == array_t({0, 1, 0, 0, 0})); // A(int)

    A::reset();
    my_construct(a, v);                       // A(const A&)
    assert(A::c == array_t({0, 0, 1, 0, 0}));

    assert(*a == v);

    A::reset();
    my_destroy(a);                            // ~A()
    assert(A::c == array_t({0, 0, 0, 0, 1}));

    A::reset();
    al.deallocate(a, s);
    assert(A::c == array_t({0, 0, 0, 0, 0}));}

void test2 () {
    A::reset();
    allocator<A> al;
    assert(A::c == array_t({0, 0, 0, 0, 0}));

    const size_t s = 3;

    A::reset();
    A* b = al.allocate(s);
    A* e = b + s;
    assert(A::c == array_t({0, 0, 0, 0, 0}));

    A::reset();
    const A v = 2;                            // A(int)
    assert(A::c == array_t({0, 1, 0, 0, 0}));

    A::reset();
    uninitialized_fill(b, e, v);
    assert(A::c == array_t({0, 0, 3, 0, 0})); // 3 A(const A&)

    assert(equal(b, e, begin({2, 2, 2})));

    A::reset();
    destroy(b, e);
    assert(A::c == array_t({0, 0, 0, 0, 3})); // 3 ~A()

    A::reset();
    al.deallocate(b, s);
    assert(A::c == array_t({0, 0, 0, 0, 0}));}

int main () {
    cout << "Allocator.cpp" << endl;
    test1();
    test2();
    cout << "Done." << endl;
    return 0;}
