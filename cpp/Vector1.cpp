// -----------
// Vector1.cpp
// -----------

// http://en.cppreference.com/w/cpp/container/vector

#include <algorithm>        // equal
#include <array>            // array
#include <cassert>          // assert
#include <cstddef>          // size_t
#include <initializer_list> // initializer_list
#include <iostream>         // cout, endl
#include <memory>           // allocator, destroy, uninitialized_copy, uninitialized_fill

using namespace std;

#define TEST1
#define TEST2
#define TEST3
#define TEST4
#define TEST5
#define TEST6

template <typename T, typename AL = allocator<T>>
class my_vector {
    friend bool operator == (const my_vector& lhs, const my_vector& rhs) {
        return (lhs.size() == rhs.size()) && equal(lhs.begin(), lhs.end(), rhs.begin());}

    friend bool operator != (const my_vector& lhs, const my_vector& rhs) {
        return !(lhs == rhs);}

    private:
        AL _al;

        T* _b = nullptr;
        T* _e = nullptr;

    public:
        my_vector () = default;

        explicit my_vector (size_t s) :
                my_vector(s, T())
            {}

        my_vector (size_t s, const T& v) {
            if (s != 0) {
                _b = _al.allocate(s);
                _e = _b + s;
                uninitialized_fill(begin(), end(), v);}}

        my_vector (initializer_list<T> rhs) {
            if (rhs.size() != 0) {
                _b = _al.allocate(rhs.size());
                _e = _b + rhs.size();
                uninitialized_copy(rhs.begin(), rhs.end(), begin());}}

        my_vector             (const my_vector&) = delete;
        my_vector& operator = (const my_vector&) = delete;

        ~my_vector () {
            destroy(_b, _e);
            _al.deallocate(_b, size());}

        T& operator [] (size_t i) {
            assert(i < size());
            return _b[i];}

        const T& operator [] (size_t i) const {
            return (*const_cast<my_vector*>(this))[i];}

        T* begin () {
            return _b;}

        const T* begin () const {
            return const_cast<my_vector*>(this)->begin();}

        T* end () {
            return _e;}

        const T* end () const {
            return const_cast<my_vector*>(this)->end();}

        size_t size () const {
            return _e - _b;}};

using array_t = array<size_t, 5>;

class A {
    friend bool operator == (const A& lhs, const A& rhs) {
        return lhs._v == rhs._v;}

    public:
        static array_t c;

        static void reset () {
            c = array_t({0, 0, 0, 0, 0});}

    private:
        int _v;

    public:
        A () :                         // default constructor
            _v (0) {
            ++c[0];}

        A (int v) :                    // int constructor
            _v (v) {
            ++c[1];}

        A (const A& rhs) :             // copy constructor
            _v (rhs._v) {
            ++c[2];}

        A& operator = (const A& rhs) { // copy assignment
            _v = rhs._v;
            ++c[3];
            return *this;}

        ~A () {                        // destructor
            ++c[4];}};

array_t A::c;

#ifdef TEST1
void test1 () {
    {
    A::reset();
    my_vector<A> x(3);
    assert(A::c == array_t({1, 0, 3, 0, 1})); // A(), 3 A(A), ~A()

    assert(x.size() == 3);
    assert(x[0]     == 0);
    assert(x[1]     == 0);
    assert(x[2]     == 0);

    A::reset();
    }
    assert(A::c == array_t({0, 0, 0, 0, 3}));} // 3 ~A()
#endif

#ifdef TEST2
void test2 () {
    {
    A::reset();
    const my_vector<A> x(3, 2);
    assert(A::c == array_t({0, 1, 3, 0, 1}));  // A(int), 3 A(A), ~A()

    assert(x.size() == 3);
    assert(x[0]     == 2);
    assert(x[1]     == 2);
    assert(x[2]     == 2);

    A::reset();
    }
    assert(A::c == array_t({0, 0, 0, 0, 3}));} // 3 ~A()
#endif

#ifdef TEST3
void test3 () {
    {
    const initializer_list<A> il = {2, 3, 4};

    A::reset();
    const my_vector<A> x = il;
    assert(A::c == array_t({0, 0, 3, 0, 0})); // 3 A(A)

    assert(x.size() == 3);
    assert(x[0]     == 2);
    assert(x[1]     == 3);
    assert(x[2]     == 4);

    A::reset();
    }
    assert(A::c == array_t({0, 0, 0, 0, 6}));} // 6 ~A()
#endif

#ifdef TEST4
void test4 () {
    my_vector<A> x = {2, 3, 4};
    assert(x[1] == 3);
    x[1] = 5;
    my_vector<A> y = {2, 5, 4};
    assert(x == y);}
#endif

#ifdef TEST5
void test5 () {
    const my_vector<A> x = {2, 3, 4};
    assert(x[1] == 3);
//  x[1] = 5;                     // error: cannot assign to return value because function 'operator[]' returns a const value
    }
#endif

#ifdef TEST6
void test6 () {
    const my_vector<A> x = {2, 3};
    const my_vector<A> y = {2, 3, 4};
    assert(x != y);
    assert(equal(begin(x), end(x), begin({2, 3})));
    assert(equal(begin(x), end(x), begin({2, 3, 4})));}
#endif

int main () {
    cout << "Vector1.cpp" << endl;
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    /*
    int n;
    cin >> n;
    switch (n) {
        #ifdef TEST1
        case 1:
            test1();
            break;
        #endif

        #ifdef TEST2
        case 2:
            test2();
            break;
        #endif

        #ifdef TEST3
        case 3:
            test3();
            break;
        #endif

        #ifdef TEST4
        case 4:
            test4();
            break;
        #endif

        #ifdef TEST5
        case 5:
            test5();
            break;
        #endif

        #ifdef TEST6
        case 6:
            test6();
            break;
        #endif
        }
    */
    cout << "Done." << endl;
    return 0;}
