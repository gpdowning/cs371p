// ---------
// Equal.cpp
// ---------

// https://en.cppreference.com/w/cpp/algorithm/equal

#include <cassert>   // assert
#include <iostream>  // cout, endl
#include <list>      // list
#include <vector>    // vector

using namespace std;

#define TEST1
#define TEST2
#define TEST3

template <typename II1, typename II2>
bool my_equal (II1 b1, II1 e1, II2 b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;}
    return true;}

#ifdef TEST1
void test1 () {
    const int  a1[] = {2, 3, 4};
    const int  a2[] = {0, 2, 3, 4, 0};
    const int* b1   = begin(a1);
    const int* e1   = end(a1);
    const int* b2   = begin(a2);
    assert(!my_equal(b1, e1, b2));}
#endif

#ifdef TEST2
void test2 () {
    const int  a1[] = {2, 3, 4};
    const int  a2[] = {0, 2, 3, 4, 0};
    const int* b1   = begin(a1);
    const int* e1   = end(a1);
    const int* b2   = begin(a2) + 1;
    assert(my_equal(b1, e1, b2));}
#endif

#ifdef TEST3
void test3 () {
    const list<int>             x1 = {2, 3, 4};
    const vector<int>           x2 = {0, 2, 3, 4, 0};
    list<int>::const_iterator   b1 = begin(x1);
    list<int>::const_iterator   e1 = end(x1);
    vector<int>::const_iterator b2 = begin(x2) + 1;
    assert(my_equal(b1, e1, b2));}
#endif

int main () {
    cout << "Equal.cpp" << endl;
    test1();
    test2();
    test3();
    /*
    int n;
    cin >> n;
    switch (n) {
        #ifdef TEST1
        case 1:
            test1());
            break;
        #endif

        #ifdef TEST2
        case 2:
            test2();
            break;
        #endif

        #ifdef TEST3
        case 3:
            test3();
            break;
        #endif

        default:
            assert(false);}
    */
    cout << "Done." << endl;
    return 0;}
