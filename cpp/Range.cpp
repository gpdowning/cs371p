// ---------
// Range.cpp
// ---------

#include <cassert>   // assert
#include <iostream>  // cout, endl
#include <stdexcept> // out_of_range

using namespace std;

template <typename II1, typename II2>
bool my_equal (II1 b1, II1 e1, II2 b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;}
    return true;}

#define TEST1
#define TEST2
#define TEST3
#define TEST4
#define TEST5
#define TEST6

template <typename T>
class range {
    public:
        class iterator {
            private:
                T _v;

            public:
                iterator (const T& v) {
                    _v = v;}

                bool operator == (const iterator& rhs) const {
                    return (_v == rhs._v);}

                bool operator != (const iterator& rhs) const {
                    return !(*this == rhs);}

                T operator * () const {
                    return _v;}

                iterator& operator ++ () {
                    ++_v;
                    return *this;}

                iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}};

    private:
        T _start;
        T _stop;

    public:
        range (const T& start, const T& stop) {
            _start = start;
            _stop  = stop;}

        bool operator == (const range& rhs) const {
            return (_start == rhs._start) && (_stop == rhs._stop);}

        bool operator != (const range& rhs) const {
            return !(*this == rhs);}

        T operator [] (int index) const {
            if ((index < 0) || (index >= (_stop - _start)))
                throw out_of_range("range");
            return _start + index;}

        iterator begin () const {
            return iterator(_start);}

        iterator end () const {
            return iterator(_stop);}

        int size () const {
            return _stop - _start;}};

#ifdef TEST1
void test1 () {
    const range<int> x(2, 2);
    assert(x.size() == 0);
    const range<int>::iterator b = begin(x); // x.begin()
    const range<int>::iterator e = end(x);   // x.end()
    assert(b == e);
    const range<int> y(2, 2);
    assert(x == y);}
#endif

#ifdef TEST2
void test2 () {
    range<int> x(2, 3);
    assert(x.size() == 1);
    range<int>::iterator b = begin(x);
    range<int>::iterator e = end(x);
    assert(b  != e);
    assert(*b == 2);
    range<int>::iterator& y = ++b;     // b.operator++()
    assert(&y == &b);
    assert(b  == e);}
#endif

#ifdef TEST3
void test3 () {
    range<int> x(2, 4);
    assert(x.size() == 2);
    range<int>::iterator b = begin(x);
    range<int>::iterator e = end(x);
    assert(b  != e);
    assert(*b == 2);
    range<int>::iterator y = b++;      // b.operator++(0)
    assert(&y != &b);
    assert(b  != e);
    assert(*b == 3);
    ++b;
    assert(b == e);}
#endif

#ifdef TEST4
void test4 () {
    range<int> x(2, 5);
    assert(x.size() == 3);
    assert(my_equal(begin(x), end(x), begin({2, 3, 4})));}
#endif

#ifdef TEST5
void test5 () {
    range<int> x(2, 5);
    int s = 0;
    for (int v : x)
        s += v;
    assert(s == 9);}
#endif

#ifdef TEST6
void test6 () {
    range<int> x(2, 5);
    assert(x[0] == 2);
    assert(x[1] == 3);
    assert(x[2] == 4);
    try {
        assert(x[3] == 5);
        assert(false);}
    catch (out_of_range& e)
        {}}
#endif

int main () {
    cout << "Range.cpp" << endl;
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    /*
    int n;
    cin >> n;
    switch (n) {
        #ifdef TEST1
        case 1:
            test1();
            break;
        #endif

        #ifdef TEST2
        case 2:
            test2();
            break;
        #endif

        #ifdef TEST3
        case 3:
            test3();
            break;
        #endif

        #ifdef TEST4
        case 4:
            test4();
            break;
        #endif

        #ifdef TEST5
        case 5:
            test5();
            break;
        #endif

        #ifdef TEST6
        case 6:
            test6();
            break;
        #endif

        default:
            assert(false);}
    */
    cout << "Done." << endl;
    return 0;}
