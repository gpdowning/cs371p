// ---------
// Array.cpp
// ---------

// https://en.cppreference.com/w/cpp/container/array
// https://gcc.gnu.org/onlinedocs/gcc-10.2.0/libstdc++/api/a00047_source.html

#include <algorithm>        // equal
#include <cassert>          // assert
#include <cstddef>          // size_t
#include <initializer_list> // initializer_list
#include <iostream>         // cout, endl

using namespace std;

#define TEST1
#define TEST2
#define TEST3
#define TEST4
#define TEST5
#define TEST6
#define TEST7
#define TEST8
#define TEST9
#define TEST10
#define TEST11





template <typename T, size_t N>
class my_array {
    friend bool operator == (const my_array& lhs, const my_array& rhs) {
        return equal(lhs.begin(), lhs.end(), rhs.begin());}

    friend bool operator != (const my_array& lhs, const my_array& rhs) {
        return !(lhs == rhs);}

private:
    T _a[N];

public:
    // my_array             (const my_array&) = default; // copy    constructor
    // my_array& operator = (const my_array&) = default; // copy    assignment
    // ~my_array            ()                = default; // destructor

    my_array () = default;

    my_array (initializer_list<T> il) {
        copy(il.begin(), il.end(), _a);}

    T& operator [] (size_t i) {
        return _a[i];}

    const T& operator [] (size_t i) const {
        return const_cast<my_array&>(*this)[i];}

    T& at (size_t i) {
        if (i >= size())
            throw out_of_range("Array::at index out of range");
        return (*this)[i];}

    const T& at (size_t i) const {
        return const_cast<my_array&>(*this).at(i);}

    T* begin () {
        return _a;}

    const T* begin () const {
        return const_cast<my_array&>(*this).begin();}

    T* end () {
        return _a + N;}

    const T* end () const {
        return const_cast<my_array&>(*this).end();}

    void fill (const T& v) {
        std::fill(begin(), end(), v);}

    size_t size () const {
        return N;}};





template <typename T, size_t N>
struct my_array_2 {
    friend bool operator == (const my_array_2& lhs, const my_array_2& rhs) {
        return equal(lhs.begin(), lhs.end(), rhs.begin());}

    friend bool operator != (const my_array_2& lhs, const my_array_2& rhs) {
        return !(lhs == rhs);}

    T _a[N];

    // my_array_2             (const my_array_2&) = default; // copy    constructor
    // my_array_2& operator = (const my_array_2&) = default; // copy    assignment
    // ~my_array_2            ()                = default; // destructor

    T& operator [] (size_t i) {
        return _a[i];}

    const T& operator [] (size_t i) const {
        return const_cast<my_array_2&>(*this)[i];}

    T& at (size_t i) {
        if (i >= size())
            throw out_of_range("Array::at index out of range");
        return (*this)[i];}

    const T& at (size_t i) const {
        return const_cast<my_array_2&>(*this).at(i);}

    T* begin () {
        return _a;}

    const T* begin () const {
        return const_cast<my_array_2&>(*this).begin();}

    T* end () {
        return _a + N;}

    const T* end () const {
        return const_cast<my_array_2&>(*this).end();}

    void fill (const T& v) {
        std::fill(begin(), end(), v);}

    size_t size () const {
        return N;}};





#ifdef TEST1
void test1 () {
    my_array<int, 3> x;
    assert(x.size() == 3);}
#endif

#ifdef TEST2
void test2 () {
    my_array<int, 3> x = {2, 3, 4};
    assert(x[1] == 3);
    x[1] = 5;
    assert(x[1] == 5);}
#endif

#ifdef TEST3
void test3 () {
    const my_array<int, 3> x = {2, 3, 4};
    assert(x[1] == 3);
//  x[1] = 5;                             // error: cannot assign to return value because function 'operator[]' returns a const value
    assert(x[1] == 3);}
#endif

#ifdef TEST4
void test4 () {
    my_array<int, 3> x = {2, 3, 4};
    assert(x.at(1) == 3);
    x.at(1) = 5;
    assert(x.at(1) == 5);
    try {
        x.at(4);}
    catch (out_of_range&)
        {}}
#endif

#ifdef TEST5
void test5 () {
    const my_array<int, 3> x = {2, 3, 4};
    assert(x.at(1) == 3);
//  x.at(1) = 5;                          // cannot assign to return value because function 'at' returns a const value
    assert(x.at(1) == 3);
    try {
        x.at(4);}
    catch (out_of_range&)
        {}}
#endif

#ifdef TEST6
void test6 () {
    const my_array<int, 3> x = {2, 3, 4};
    assert(equal(begin(x), end(x), begin({2, 3, 4})));}
#endif

#ifdef TEST7
void test7 () {
    const my_array<int, 3> x = {2, 3, 4};
    const my_array<int, 3> y = x;
    assert(&*begin(x) != &*begin(y));
    assert(equal(begin(y), end(y), begin({2, 3, 4})));}
#endif

#ifdef TEST8
void test8 () {
    const my_array<int, 3> x = {2, 3, 4};
    my_array<int, 3>       y = {5, 6, 7};
    y = x;
    assert(&*begin(x) != &*begin(y));
    assert(equal(begin(y), end(y), begin({2, 3, 4})));}
#endif

#ifdef TEST9
void test9 () {
    const my_array<int, 5> x = {2, 3, 4, 5, 6};
    const my_array<int, 5> y = {2, 3, 4, 5, 6};
    assert(  x == y);
    assert(!(x != y));}
#endif

#ifdef TEST10
void test10 () {
    const my_array<int, 5> x = {2, 3, 4, 5, 6};
    const my_array<int, 5> y = {2, 3, 4, 5, 7};
    assert(  x != y);
    assert(!(x == y));}
#endif

#ifdef TEST11
void test11 () {
    my_array<int, 3>       x = {2, 3, 4};
    const my_array<int, 3> y = {5, 5, 5};
    x.fill(5);
    assert(  x == y);
    assert(!(x != y));}
#endif

int main () {
    cout << "Array.cpp" << endl;
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    test9();
    test10();
    test11();
/*
    int n;
    cin >> n;
    switch (n) {
        #ifdef TEST1
        case 1:
            test1();
            break;
        #endif

        #ifdef TEST2
        case 2:
            test2();
            break;
        #endif

        #ifdef TEST3
        case 3:
            test3();
            break;
        #endif

        #ifdef TEST4
        case 4:
            test4();
            break;
        #endif

        #ifdef TEST5
        case 5:
            test5();
            break;
        #endif

        #ifdef TEST6
        case 6:
            test6();
            break;
        #endif

        #ifdef TEST7
        case 7:
            test7();
            break;
        #endif

        #ifdef TEST8
        case 8:
            test8();
            break;
        #endif

        #ifdef TEST9
        case 9:
            test9();
            break;
        #endif

        #ifdef TEST10
        case 10:
            test10();
            break;
        #endif

        #ifdef TEST11
        case 11:
            test11();
            break;
        #endif

        default:
            assert(false);}
*/
    cout << "Done." << endl;
    return 0;}
