// ---------
// Hello.cpp
// ---------

// https://en.cppreference.com
// https://en.cppreference.com/w/cpp/io
// https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines.html

#include <iostream> // cout, endl

int main () {
    using namespace std;
    cout << "Nothing to be done." << endl;
    return 0;}

/*
Developed in 1985 by Bjarne Stroustrup of Denmark.
C++ is procedural, object-oriented, statically typed, and not garbage collected.

C++98
C++03
C++11
C++14
C++17
C++20
C++23
C++27



% which clang++
/usr/local/opt/llvm/bin//clang++



% clang++ --version
Homebrew clang version 19.1.6



# --coverage: enable gcov
# -g:         generate debug information
# -std:       specify the language standard to compile for
# -Wall:      all warnings
# -Wextra:    extra warnings
# -Wpedantic: pedantic warnings
# -o:         specify output file
# -l          specify libraries
% clang++ --coverage -g -std=c++20 -Wall -Wextra -Wpedantic Hello.cpp -o Hello -lgtest -lgtest_main



% ./Hello
Nothing to be done.
*/
