// -----------------------
// OperatorOverloading.cpp
// -----------------------

#include <cassert>  // assert
#include <iostream> // cout, endl, istream, ostream

using namespace std;

template <typename T>
struct A {
    A (const T&)
        {}

    istream& operator >> (istream& rhs) {
        return rhs;}

    ostream& operator << (ostream& rhs) const {
        return rhs;}

    bool operator == (const A&) const {
        return true;}};

void test1 () {
    A<int> x = 2;

//  cin  >> x;          // error: invalid operands to binary expression ('istream' (aka 'basic_istream<char>') and 'A<int>')
//  cout << x;          // error: invalid operands to binary expression ('ostream' (aka 'basic_ostream<char>') and 'A<int>')

    const A<int> y = 2;
    const A<int> z = 2;

    assert(y == z);     // x.operator==(y)
    assert(y == 2);     // x.operator==(2)
    assert(2 == z);}    // 2.operator==(x) -> x.operator==(2)



template <typename T>
struct B {
    friend istream& operator >> (istream& lhs, B&) {
        return lhs;}

    friend ostream& operator << (ostream& lhs, const B&) {
        return lhs;}

    friend bool operator == (const B&, const B&) {
        return true;}

    B (const T&)
        {}};

void test2 () {
    B<int> x = 2;

    cin  >> x;
    cout << x;

    const B<int> y = 2;
    const B<int> z = 2;

    assert(y == z);     // operator==(x, y)
    assert(y == 2);     // operator==(x, 2)
    assert(2 == z);}    // operator==(2, x)



template <typename T>
struct C {
    C (const T&)
        {}};

template <typename T>
istream& operator >> (istream& lhs, C<T>&) {
    return lhs;}

template <typename T>
ostream& operator << (ostream& lhs, const C<T>&) {
    return lhs;}

template <typename T>
bool operator == (const C<T>&, const C<T>&) {
    return true;}

void test3 () {
    C<int> x = 2;

    cin  >> x;
    cout << x;

    const C<int> y = 2;
    const C<int> z = 2;

    assert(y == z);     // operator==(x, y)
//  assert(y == 2);     // error: invalid operands to binary expression ('const C<int>' and 'int')
//  assert(2 == z);     // error: invalid operands to binary expression ('int' and 'const C<int>')
    }



int main () {
    using namespace std;
    cout << "OperatorOverloading.cpp" << endl;
    test1();
    test2();
    test3();
    cout << "Done." << endl;
    return 0;}
