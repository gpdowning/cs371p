// -----------
// Mon, 27 Jan
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 2 Feb: Blog  #3
    Sun, 2 Feb: Paper #3

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Mon, 27 Jan, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/

    Fri, 31 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/
*/





/*
last time
    error handling without exceptions
    use the return
    use a global
    use an argument (be careful)

this time
    values
    addresses
    references
    call by value
    call by address
    call by reference
    exceptions
    catch by reference
*/





/*
two tokens
    *, &
two contexts
    modifying a variable
    modifying a type
*/






int i = 2;
int v = i; // v is a copy of i
++v;
cout << i; // 2





int  i = 2;
int* p = i;   // no
int* p = &i;  // &: take the address of; it must have an l-value
// ++p;
// cout << i; // 2
++*p;         // *: dereference; it must have an address
cout << i;    // 3

int** q = &p; // pointer to a pointer to an int

int* r;       // initialization is NOT required
r = &i;
++*r;
int j = 5;    // pointers can be reassigned
r = &j;





int  i = 2;
int& r;      // no, must be initialized
int& r = &i; // no
int& r = i;  // must be given an l-value

cout << (&r == &i); // true






int f (int v) { // pass by value
    ++v;}

int i = 2;
f(i);
cout << i; // 2





int g (int* p) { // pass by address
//    ++p;       // usually not what you want
    ++*p;}

int j = 3;
g(j);      // no
g(&j);
cout << j; // 4
g(185);    // no
g(0);      // yes!!!





int h (int& r) { // pass by reference
    ++*r;        // no
    ++r;}

int k = 4;
h(&k);     // no
h(k);
cout << k; // 5
h(185);    // no
h(0);      // no





int f (...) {
    ...
    if (<something wrong>)
        throw E(...);
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (E e) {
        ...}
    ...}

/*
f does NOT throw E
rest of f
rest of try
NO catch E
rest of g
*/

/*
f does throw E
NO rest of f
NO rest of try
catch E
rest of g
*/





int f (...) {
    ...
    if (<something wrong>)
        throw E2(...)
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (E& e) {
        ...}
    ...}

/*
f raises E2
NO rest of f
NO rest of try
NO catch E
NO rest of g
*/





int f (...) {
    ...
    if (<something wrong>)
        throw tiger(...)   // throw always copies
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (mammal e) { // catch by value, second copy, only mammal part of the tiger
        ...}
    ...}





int f (...) {
    ...
    if (<something wrong>)
        tiger x(...);
        throw &x;
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (mammal* e) { // catch by address, dead tiger
        ...}
    ...}





int f (...) {
    ...
    if (<something wrong>)
        throw tiger(...)
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (mammal& e) { // always catch by reference
        ...}
    ...}





int f (...) {
    ...
    if (<something wrong>)
        throw tiger(...)
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (mammal& e) {
        ...}
    catch (tiger& e) {  // never runs
        ...}
    ...}





int f (...) {
    ...
    if (<something wrong>)
        throw tiger(...)
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (tiger& e) {  // put children first
        ...}
    catch (mammal& e) {
        ...}
    ...}
