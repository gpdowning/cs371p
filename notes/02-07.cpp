// -----------
// Fri,  7 Feb
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 9 Feb: Blog  #4
    Sun, 9 Feb: Paper #4

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri,  7 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 10 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 12 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/
*/





/*
last time
    Project #2: Voting

this time
    Exercise #3: reverse()
*/





template <typename II1, typename II2>
bool my_equal (II1 b1, II1 e1, II2 b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;}
    return true;}





template <typename II, typename OI>
OI copy (II b, II e, OI x) {
    while (b != e) {
        *x = *b;
        ++b;
        ++x;}
    return x;}





/*
input iterator
    ==, !=, ++, * (read only)
*/

/*
output iterator
    ++, * (write only)
*/

/*
forward iterator
    ==, !=, ++, * (read/write)
*/

/*
bidirectional iterator
    ==, !=, ++, --, * (read/write)
*/

/*
random access iterator
    ==, !=, ++, --, * (read/write), <, <=, >, >=, [], +, -
*/
