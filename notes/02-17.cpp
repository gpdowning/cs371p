// -----------
// Mon, 17 Feb
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 9 Feb: Blog  #5
    Sun, 9 Feb: Paper #5

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Mon, 17 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Fri, 21 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Wed, 26 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #3
    https://www.cs.utexas.edu/~utpc/
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/





/*
last time
    arrays

this time
    implicit type conversion
    explicit constructors
    operator overloading
    friends
    initializations
*/





/*
struct has default access of private
class  has default access of public

by convention, if everything is public, use struct
*/





struct A {
    private:
        int _i;

    A (int i) {
        _i = i;}

    bool operator == (const A& rhs) const {
        return (_i == rhs._i);}};

A x(2);
A y = 3;

f(x);
f(4);

cout << (x == y); // x.operator==(y)
cout << (x == 2); // x.operator==(2)
cout << (2 == x); // no





struct A {
    private:
        int _i;

    explicit A (int i) {
        _i = i;}

    bool operator == (const A& rhs) const {
        return (_i == rhs._i);}};

void f (A z) {}

A x(2);
A y = 3; // no

f(x);
f(4); // no

cout << (x == y); // x.operator==(y)
cout << (x == 2); // no
cout << (2 == x); // no





struct A {
    friend bool operator == (const A&, const A&);

    private:
        int _i;

    A (int i) {
        _i = i;}};

bool operator == (const A& lhs, const A& rhs) {}
    return (lhs._i == rhs._i);}};

void f (A z) {}

A x(2);
A y = 3;

f(x);
f(4);

cout << (x == y); // operator==(x, y)
cout << (x == 2); // operator==(x, 2)
cout << (2 == x); // operator==(2, x)





struct A {
    friend bool operator == (const A& lhs, const A& rhs) {
        return (lhs._i == rhs._i);}

    private:
        int _i;

    A (int i) {
        _i = i;}};

void f (A z) {}

A x(2);
A y = 3;

f(x);
f(4);

cout << (x == y); // operator==(x, y)
cout << (x == 2); // operator==(x, 2)
cout << (2 == x); // operator==(2, x)





// -------------------
// Initializations.cpp
// -------------------

void test1 () {
    int i(2);
    assert(i == 2);

    int j = 2;
    assert(j == 2);

    int k{2};
    assert(k == 2);

    int l = {2};
    assert(l == 2);}



void test2 () {
    int i(2.0);
    assert(i == 2);

    int j = 2.0;
    assert(j == 2);

//  int k{2.0};     // error: narrowing conversion of '2.0e+0' from 'double' to 'int'

//  int l = {2.0};  // error: narrowing conversion of '2.0e+0' from 'double' to 'int'
    }



struct A {
    int i;

//  A             ()         = default; // default constructor
//  A             (const A&) = default; // copy constructor
//  A& operator = (const A&) = default; // copy assignment operator
//  ~A            ()         = default; // destructor
    };

void test3 () {
//  A x(2);      // error: no matching constructor for initialization of 'A'

//  A y = 2;     // error: no viable conversion from 'int' to 'A'

    A z{2};
    assert(&z);

    A t = {2};
    assert(&t);}



struct B {
    static int c;

    int i;

    B             ()         = default; // default constructor
    B             (const B&) = default; // copy constructor
    B& operator = (const B&) = default; // copy assignment operator
    ~B            ()         = default; // destructor

    B (int) {
        ++c;}};

int B::c = 0;

void test4 () {
    B x(2);             // B(int)
    assert(B::c == 1);

    B y = 2;
    assert(B::c == 2);  // B(int)

    B z{2};             // B(int)
    assert(B::c == 3);

    B t = {2};          // B(int)
    assert(B::c == 4);}
