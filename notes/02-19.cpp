// -----------
// Wed, 19 Feb
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 9 Feb: Blog  #5
    Sun, 9 Feb: Paper #5

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 21 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 24 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 26 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #3
    https://www.cs.utexas.edu/~utpc/
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/





/*
last time
    implicit type conversion
    explicit constructors
    operator overloading
    friends
    initializations

this time
    initializations
    std::initializer_list
*/





// -------------------
// Initializations.cpp
// -------------------

struct A {
    int i;

//  A             ()         = default; // default constructor
//  A             (const A&) = default; // copy constructor
//  A& operator = (const A&) = default; // copy assignment operator
//  ~A            ()         = default; // destructor
    };

void test3 () {
//  A x(2);      // error: no matching constructor for initialization of 'A'

//  A y = 2;     // error: no viable conversion from 'int' to 'A'

    A z{2};
    assert(&z);

    A t = {2};
    assert(&t);}



struct B {
    static int c;

    const int i = 0;

    B             ()         = default; // default constructor
    B             (const B&) = default; // copy constructor
    B& operator = (const B&) = default; // copy assignment operator
    ~B            ()         = default; // destructor

    B (int) {
        ++c;}};

int B::c;

void test4 () {
    B x(2);             // B(int)
    assert(B::c == 1);

    B y = 2;
    assert(B::c == 2);  // B(int)

    B z{2};             // B(int)
    assert(B::c == 3);

    B t = {2};          // B(int)
    assert(B::c == 4);}



struct C {
    static int c;

    int i;

    C             ()         = default; // default constructor
    C             (const C&) = default; // copy constructor
    C& operator = (const C&) = default; // copy assignment operator
    ~C            ()         = default; // destructor

    explicit C (int) {
        ++c;}};

int C::c = 0;

void test5 () {
    C x(2);            // C(int)
    assert(C::c == 1);

//  C y = 2;           // error: no viable conversion from 'int' to 'C'

    C z{2};            // C(int)
    assert(C::c == 2);

//  C t = {2};         // error: chosen constructor is explicit in copy-initialization
    }



struct D {
    static int c;

    int i;

    D             ()         = default; // default constructor
    D             (const D&) = default; // copy constructor
    D& operator = (const D&) = default; // copy assignment operator
    ~D            ()         = default; // destructor

    D (initializer_list<int>) {
        ++c;}};

int D::c = 0;

void test6 () {
//  D x(2);             // error: no matching constructor for initialization of 'D'

//  D y = 2;            // error: no viable conversion from 'int' to 'D'

    D z{2};             // D(initializer_list<int>)
    assert(D::c == 1);

    D t = {2};          // D(initializer_list<int>)
    assert(D::c == 2);}



struct E {
    static int c;

    int i;

    E             ()         = default; // default constructor
    E             (const E&) = default; // copy constructor
    E& operator = (const E&) = default; // copy assignment operator
    ~E            ()         = default; // destructor

    explicit E (initializer_list<int>) {
        ++c;}};

int E::c = 0;

void test7 () {
//  E x(2);            // error: no matching constructor for initialization of 'E'

//  E y = 2;           // error: no viable conversion from 'int' to 'E'

    E z{2};            // E(initializer_list<int>)
    assert(E::c == 1);

//  E t = {2};         // error: chosen constructor is explicit in copy-initialization
    }



struct F {
    static int c;

    int i;

    F             ()         = default; // default constructor
    F             (const F&) = default; // copy constructor
    F& operator = (const F&) = default; // copy assignment operator
    ~F            ()         = default; // destructor

    F (int) {
        c += 2;}

    F (initializer_list<int>) {
        c += 3;}};

int F::c = 0;

void test8 () {
    F x(2);              // F(int)
    assert(F::c == 2);

    F y = 2;             // F(int)
    assert(&y);
    assert(F::c == 4);

    F z{2};              // F(initializer_list<int>)
    assert(F::c == 7);

    F t = {2};           // F(initializer_list<int>)
    assert(F::c == 10);}

void test9 () {
    vector<int> x(2);                               // vector<int>(int)
    assert(x.size() == 2);
    assert(equal(begin(x), end(x), begin({0, 0})));

//  vector<int> y = 2;                              // error: conversion from 'int' to non-scalar type 'std::vector<int>' requested

    vector<int> z{2};                               // vector<int>(initializer_list<int>)
    assert(z.size() == 1);
    assert(equal(begin(z), end(z), begin({2})));

    vector<int> t = {2};                            // vector<int>(initializer_list<int>)
    assert(t.size() == 1);
    assert(equal(begin(t), end(t), begin({2})));}





// -------------------
// InitializerList.cpp
// -------------------

void test1 () {
    initializer_list<int> x;
    assert(x.size() == 0);

//  initializer_list<int> y();    // warning: empty parentheses interpreted as a function declaration [-Wvexing-parse]

    initializer_list<int> z{};
    assert(z.size() == 0);

    initializer_list<int> t = {};
    assert(t.size() == 0);}

int a[] = {2, 3, 4};

void test2 () {
//  initializer_list<int> x(2);  // error: no matching function for call to 'std::initializer_list<int>::initializer_list(int)'

//  initializer_list<int> y = 2; // error: conversion from 'int' to non-scalar type 'std::initializer_list<int>' requested

    initializer_list<int> z{2};
    assert(z.size() == 1);

    initializer_list<int> t = {2};
    assert(t.size() == 1);}

void test3 () {
    initializer_list<int> z{2, 3};
    assert(z.size() == 2);

    initializer_list<int> t = {2, 3};
    assert(t.size() == 2);}

void test4 () {
    initializer_list<int> x = {2, 3, 4};
    initializer_list<int> y = x;
    assert(equal(begin(x), end(x), begin(y)));}

void test5 () {
    initializer_list<int> x = {2, 3, 4};
    initializer_list<int> y = {5, 6};
    x = y;                                      // well-defined
    assert(x.size() == 2);
    assert(equal(begin(x), end(x), begin(y)));}

void test6 () {
    initializer_list<int> x = {2, 3, 4};
    x = {5, 6};
    assert(x.size() == 2);
    assert(&x);}

void test7 () {
    initializer_list<int> x = {2, 3, 4};
    initializer_list<int> y = {2, 3, 4};
//  assert(x == y);                             //  error: no match for 'operator==' (operand types are 'std::initializer_list<int>' and 'std::initializer_list<int>')
    assert(equal(begin(x), end(x), begin(y)));}

void test8 () {
    initializer_list<int> x = {2, 3, 4};
    vector<int>           y = x;
    assert(equal(begin(x), end(x), begin(y)));}

void test9 () {
    vector<int>           x = {2, 3, 4};
    initializer_list<int> y = {5, 6};
    x = y;
//  y = x;                                       // error: no match for 'operator=' (operand types are 'std::initializer_list<int>' and 'std::vector<int>')
    assert(equal(begin(x), end(x), begin(y)));}

void test10 () {
    vector<int> x = {2, 3, 4};
    x = {5, 6};
    assert(equal(begin(x), end(x), begin({5, 6})));}

void test11 () {
    vector<int> x = {2, 3, 4};
    assert(x == {2, 3, 4});          // error: initializer list cannot be used on the right hand side of operator '=='
    assert(x == vector({2, 3, 4}));}
