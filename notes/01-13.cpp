// -----------
// Mon, 13 Jan
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 19 Jan: Blog  #1
    Sun, 19 Jan: Paper #1: Syllabus

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Mon, 13 Jan, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Fri, 17 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Wed, 22 Jan, 6 pm, GDC 1.406
    UTPC: UT Programming Contest
    Beginner's Workshop
    https://www.cs.utexas.edu/~utpc/

    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/
*/





/*
class website
    https://www.cs.utexas.edu/users/downing/cs371p/

Canvas
    grades
    please check your grades regularly
    Lectures Online
    quizzes

Ed Discussion
    threaded questions
    private posts to staff
    please be proactive

Ed Lessons
    exercises

GitLab
    class notes
    code examples
    exercise solutions
    project skeletons

HackerRank
    projects

Perusall
    papers
*/




/*
projects
    5, 0-2
    every three weeks
    first one is individual
    rest can be in pairs, one MUST be in pairs
    semi auto-graded on HackerRank
    HackerRank only grades the correctness of the code
    documentation, testing, coverage, continuous integration, issue tracker
    late up to 2 days, twice
    no make-ups
    resubmits within a week, twice
    no 3s

exercises
    12, 0-3
    ~20 min
    auto-graded on Ed Lessons
    have TA review your code before you submit
    no lates
    make-ups within 2 days, twice, but not the last
    no re-submits
    2 3s will make up 1

blogs
    14, 0-2
    prompt posted on Fri, due on Sun
    on any platform, most use Medium
    confirm the word count, confirm that you responded to the prompts
    late up to 2 days, twice
    no make-ups
    no re-submits
    no 3s

papers
    14, 0-3
    posted on Mon, due on Sun
    auto-graded on Perusall
    monitor the annotations, answer each others questions
    late up to 2 days, twice in the term
    no make-ups
    no re-submits
    2 3s will make up 1

quizzes
    41, 0-3
    auto-graded on Canvas
    2 min after the hour
    3 min
    no lates
    make-ups within 2 days, five times, but not the last
    no re-submits
    2 3s will make up 1
*/





/*
ice breaker
    name
    contact info
    where did you grow up
    where did you go to high school
    what's your favorite programming language and why
    what led you to major in CS, when did you decide
    google "cs 371p fall 2024 final entry", click images
*/





/*
UTPC
    competitive programming

    local contests
    regional contests
    world contests

    2014: 2nd, Rice, 1st, Thailand
    2015: 2nd, Rice, 1st, Morocco, our 4 teams were in the top 7
    2016: 1st, 2nd, 3rd, 4th, ???
    2017: Beijing
    2018: Portugal
    2020: Moscow, online, a year late
    2021: didn't make it, Dhaka, a year late
    2023: Luxor, Egypt, a year late
    2023: didn't make it, Luxor, Egypt, a year late
    2024: Astana, Kazakhstan in Sep
    2025: ??? in Sep
*/





// ---------
// Hello.cpp
// ---------

// https://en.cppreference.com
// https://en.cppreference.com/w/cpp/io
// https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines.html

#include <iostream> // cout, endl // no analogy to anything in Java

int main () {
    using namespace std;                   // analagous to Java's import
    cout << "Nothing to be done." << endl;
    return 0;}

/*
Developed in 1985 by Bjarne Stroustrup of Denmark.
C++ is procedural, object-oriented, statically typed, and not garbage collected.

C++98
C++03
C++11
C++14
C++17
C++20
C++23
C++27



% which clang++
/usr/local/opt/llvm/bin//clang++



% clang++ --version
Homebrew clang version 19.1.6



# --coverage: enable gcov
# -g:         generate debug information
# -std:       specify the language standard to compile for
# -Wall:      all warnings
# -Wextra:    extra warnings
# -Wpedantic: pedantic warnings
# -o:         specify output file
# -l          specify libraries
% clang++ --coverage -g -std=c++20 -Wall -Wextra -Wpedantic Hello.cpp -o Hello -lgtest -lgtest_main



% ./Hello
Nothing to be done.
*/
