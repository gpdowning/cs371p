// -----------
// Wed, 26 Feb
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 3 Mar: Blog  #7
    Sun, 3 Mar: Paper #7

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Wed, 26 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #3
    https://www.cs.utexas.edu/~utpc/

    Fri, 28 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 3 Mar, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/





/*
last time
    arrays on the heap

this time
*/





// -----------
// Arrays2.cpp
// -----------

void h1 (vector<int>& y) {
    vector<int>::iterator p = begin(y);
    ++p;
    ++p[0];
    ++*p;}

void test9 () {
    vector<int> x = {2, 3, 4};
    h1(x);
    assert(equal(begin(x), end(x), begin({2, 5, 4})));}

void h2 (vector<int> y) {
    vector<int>::iterator p = begin(y);
    ++p;
    ++p[0];
    ++*p;}

void test10 () {
    vector<int> x = {2, 3, 4};
    h2(x);
    assert(equal(begin(x), end(x), begin({2, 3, 4})));}





T  v = ...;      // T(...)
T* a = new T[s]; // O(n): T(),  s times
fill(a, a+s, v); // O(n): =(T), s times
...
delete [] a;     // O(n): ~T(), s times





allocator<T> al;
T* a = al.allocate(s);    // O(1)
loop
    al.construct(a+i, v); // O(n): T(T)
...
loop
    al.destroy(a+i);      // O(n): ~T()
al.deallocate(a);         // O(1)





template <typename T, size_t N>
class my_allocator {
    private:
        char _a[N];

    public:
        class iterator {
            ...}

        class const_iterator {
            ...}

        ...};





my_allocator<double, 100> al;

/*
92...92
*/





double* a = al.allocate(5); // O(n); find first free block that's big enough

/*
48,       52
-40...-40,44...44
*/





double* b = al.allocate(3);

/*
48,       32,       20
-40...-40,-24...-24,12...12
*/





double* c = al.allocate(3); // fails, no free block is big enough

/*
48,       32,       20
-40...-40,-24...-24,12...12
*/





double* c = al.allocate(2); // succeeds, but there isn't enough room for a remaining free block

/*
48,       32,       20
-40...-40,-24...-24,-12...-12
*/





al.deallocate(a);

/*
48,     32,       -20
40...40,-24...-24,-12...-12
*/
