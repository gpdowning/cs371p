// -----------
// Mon, 24 Feb
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 3 Mar: Blog  #7
    Sun, 3 Mar: Paper #7

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Mon, 24 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 26 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #3
    https://www.cs.utexas.edu/~utpc/

    Fri, 28 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/





/*
last time
    Exercise #5: Array

this time
    arrays on the heap
*/





// -----------
// Arrays2.cpp
// -----------

void f (int p[]) {
    assert(sizeof(p) == 8); // warning: sizeof on array function parameter will return size of 'int *' instead of 'int []'
    ++p;
    ++p[0];
    ++*p;}

void test1 () {
    const ptrdiff_t s = 10;
    const int       v =  2;
    int*  const     a = new int[s];  // O(1)
    int*  const     a = new T[s];    // O(n): T(), s times
    assert(sizeof(a) == 8);
    fill(a, a + s, v);               // O(n): =(T),  s times
    assert(count(a, a + s, v) == s); // O(n): ==(T), s times
    assert(a[1] == v);
    f(a);
    assert(a[1] == v + 2);
//  delete    a;                     // undefined
    delete [] a;}                    // O(n): ~T(), s times

void g (int* p) {
    assert(sizeof(p) == 8);
    ++p;
    ++p[0];
    ++*p;}

void test2 () {
    const ptrdiff_t s = 10;
    const int       v = 2;
    int* const      a = new int[s];
    assert(sizeof(a) == 8);
    fill(a, a + s, v);
    assert(count(a, a + s, v) == s);
    assert(a[1] == v);
    g(a);
    assert(a[1] == v + 2);
    delete [] a;}

void test3 () {
    const size_t  s = 10;
    const int     v = 2;
    int*  const   a = new int[s];
    assert(sizeof(a) == 8);
    fill(a, a + s, v);
    int* const b = a;             // O(1)
    assert(&a[1] == &b[1]);
    delete [] a;}

void test4 () {
    const size_t  s = 10;
    const int     v = 2;
    int*  const   a = new int[s];
    assert(sizeof(a) == 8);
    fill(a, a + s, v);
    int* const x = new int[s];
    copy(a, a + s, x);
    assert( a[1] ==  x[1]);
    assert(&a[1] != &x[1]);
    delete [] a;
    delete [] x;}

void test5 () {
    const size_t s = 10;
    const int    v =  2;
    int*  const  a = new int[s];
    assert(sizeof(a) == 8);
    fill(a, a + s, v);
    int* b = new int[s];
    fill(b, b + s, v);
//  b = a;                       // memory leak
    copy(a, a + s, b);
    assert( a[1] ==  b[1]);
    assert(&a[1] != &b[1]);
    delete [] a;
    delete [] b;}

struct Mammal {
    int i;

    string f () {
        return "A::f";}};

struct Tiger: Mammal {
    int j;

    string f () {
        return "B::f";}};

void test6 () {
//  B* const a = new A[10];     // error: invalid conversion from ‘A*’ to ‘B*’
    A* const a = new B[10];     // dangerous
    assert(a[0].f() == "A::f");
//  assert(a[1].f() == "A::f"); // undefined
//  delete [] a;                // undefined
    B* b = static_cast<B*>(a);
    assert(b[1].f() == "B::f");
    delete [] b;}               // ~B::B() and ~A::A()

void test7 () {
    const ptrdiff_t   s = 10;
    const int         v =  2;
    unique_ptr<int[]> a(new int[s]);
    fill(a.get(), a.get() + s, v);
    assert(count(a.get(), a.get() + s, v) == s);
    assert(a.get()[1] == v);
    f(a.get());
    assert(a.get()[1] == v + 2);}





template <typename T>
class vector {
    private:
        T* b;
        T* e;

    public:
        vector (int s, const T& v) {
            b = new T[s];             // O(n): T(),  s times
            e = b + s;
            fill(b, e, v);}           // O(n): =T(), s times

        int size () const {
            return e - b;}
        ...};





void test8 () {
    const size_t s = 10;
    const int    v =  2;
    vector<int>  x(s, v);              // O(n): T(T), s times
    assert(x.size() == s);
    assert(x[0]     == v);
    vector<int> y(x);                  // O(n): T(T),  s times
    assert(x          ==   y);         // O(n): ==(T), s times
    assert(&*begin(x) != &*begin(y));
    vector<int> z(2 * s, v);
    x = z;                             // O(n): ~T(), 10 times; T(T), 20 times
    assert(x          ==  z);
    assert(&*begin(x) != &*begin(z));}
