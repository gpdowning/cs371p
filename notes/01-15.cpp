// -----------
// Wed, 15 Jan
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 19 Jan: Blog  #1
    Sun, 19 Jan: Paper #1: Syllabus

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 17 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Wed, 22 Jan, 6 pm, GDC 1.406
    UTPC: UT Programming Contest
    Beginner's Workshop
    https://www.cs.utexas.edu/~utpc/

    Mon, 27 Jan, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/
*/





// input

2

2 2 2 2 0
2 1 1 2 2 2 2 2 2 3 3 0
1 2 2 2 2 2 2 2 2 2 2 2 0 3
2 2 2 1 2 2 2 2 2 3 2 3 0 0
2 2 2 2 1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 3 2 2 2 3 2 2 3 2 3 2 2 2 0 0 0 0 0 0 0 0

3 3 3 3 2
1 3 2 1 2 3 2 3 3 0 3 0
3 2 2 3 3 2 3 3 3 2 2 3 2 3
2 3 3 2 3 3 2 2 2 2 3 3 2 2
2 3 1 2 2 2 3 1 1 3 1 3 1 1 3 1 2 1 2 3 3 3 1 2 3 1 1 2 3 3 1 2 1 3 1 3 3 2 3 2 2 1

// output

B-
B-





// read, eval, print loop (REPL)

/*
kernel (Grades.hpp)
run harness (run_Grades.cpp)
test harness (test_Grades.cpp)

hr_Grades.cpp (combine Grades.hpp run_Grades.cpp, remove #include)
*/





// ----------
// Grades.hpp
// ----------

string grades_eval (const vector<vector<int>>& v_v_scores) {
    assert(&v_v_scores);
    return "B-";}





// ---------------
// test_Grades.cpp
// ---------------

TEST(grades_eval, test_0) {
    const vector<vector<int>> v_v_scores = {{0, 0, 0, 0, 0}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "B-");}





// --------------
// run_Grades.cpp
// --------------

vector<vector<int>> grades_read () {
    // read newline
    string s;
    getline(cin, s);

    // v_scores
    vector<vector<int>> v_v_scores(5);

    // read scores
    for (int i = 0; i != 5; ++i) {
        getline(cin, s);
        istringstream iss(s);
        copy(istream_iterator<int>(iss), istream_iterator<int>(), back_inserter(v_v_scores[i]));}

    return v_v_scores;}

void grades_print (const string& letter) {
    cout << letter << endl;}

int main () {
    // read number of test cases
    int n;
    cin >> n;

    // read newline
    string s;
    getline(cin, s);

    // read, eval, print (REPL)
    while (true) {
        const vector<vector<int>> v_v_scores = grades_read();
        if (!cin)
            break;
        const string letter = grades_eval(v_v_scores);
        grades_print(letter);}
    return 0;}





/*
Canvas
    assignment
    submit GitLab URL
    Academic Integrity Quiz
    don't do this until you're sure you've done everything

HackerRank
    contest
    submit as many times as you like
    4 tests, 3 tests to be able to resubmit

GitLab
    public code repo
        fork and clone

    public test repo
        between 200 and 300 tests
        merge request of your tests

    private code repo
        cppcheck, gcov, astyle, valgrind
        between 10 and 20 unit tests
        issue tracker (import issues) plus at least 5 more
        at least 5 commits
        pipeline
        README
            completion time (estimated and actual)
            git SHA
            Comments
                ChatGPT (how did you use it)

MOSS
*/
