// -----------
// Wed, 22 Jan
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 26 Jan: Blog  #2
    Sun, 26 Jan: Paper #2: Makefile

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Wed, 22 Jan, 6 pm, GDC 1.406
    UTPC: UT Programming Contest
    Beginner's Workshop
    https://www.cs.utexas.edu/~utpc/

    Fri, 24 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 27 Jan, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/
*/





/*
last time
    finished talking about the grades

this time
    assertions
    unit tests
    coverage
*/





/*
Collatz Conjecture
about 100 years old

take a pos int
if even, divide by 2
otherwise, multiply by 3 and add 1
repeat until 1

5 16 8 4 2 1

the cycle length of  1 is 1
the cycle length of  5 is 6
the cycle length of 10 is 7
*/





l-value = r-value
i = 2;
2 = i; // no

>, <, ==, %, ...   // take two r-values, return an r-value

/=, *=, +=, =, ... // take an l-value and an r-value
// Python  returns nothing
// C, Java return an r-value
// C++     returns an l-value





// --------------
// Assertions.cpp
// --------------

// https://en.cppreference.com/w/cpp/error/assert

#include <cassert>  // assert
#include <iostream> // cout, endl

using namespace std;

int cycle_length (int n) {
    assert(n > 0); // precondition
    int c = 0; // should be 1
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0); // postcondition
    return c;}

void test () {
    assert(cycle_length( 1) == 1);
    assert(cycle_length( 5) == 6);
    assert(cycle_length(10) == 7);}

int main () {
    cout << "Assertions.cpp" << endl;
    test();
    cout << "Done." << endl;
    return 0;}

/*
% clang++ --coverage -g -std=c++20 -Wall -Wextra -Wpedantic Assertions.cpp -o Assertions -lgtest -lgtest_main



% ./Assertions
Assertions.cpp
Assertion failed: (c > 0), function cycle_length, file Assertions.cpp, line 21.
*/



/*
Turn OFF assertions at compile time with -DNDEBUG
% clang++ --coverage -g -std=c++20 -DNDEBUG -Wall -Wextra -Wpedantic Assertions.cpp -o Assertions -lgtest -lgtest_main



% ./Assertions
Assertions.cpp
Done.
*/





/*
assertions are good for
    preconditions
    postconditions
    loop invariants

not good for
    testing,    instead, unit test framework
    user input, instead, exceptions
*/





// --------------
// UnitTests1.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 0;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(UnitTestsFixture, test0) {
    ASSERT_EQ(cycle_length( 1), 1);}

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 5), 6);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% clang++ --coverage -g -std=c++20 -Wall -Wextra -Wpedantic UnitTests1.cpp -o UnitTests1 -lgtest -lgtest_main



% ./UnitTests1
Running main() from /tmp/googletest-20230802-5079-wamwr8/googletest-1.14.0/googletest/src/gtest_main.cc
[==========] Running 3 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test0
Assertion failed: (c > 0), function cycle_length, file UnitTests1.cpp, line 22.
*/





// --------------
// UnitTests2.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(UnitTestsFixture, test0) {
    ASSERT_EQ(cycle_length( 1), 1);}

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 5), 5);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% ./UnitTests2
Running main() from /tmp/googletest-20210612-68642-1k2ka09/googletest-release-1.12.1/googletest/src/gtest_main.cc
[==========] Running 3 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test0
[       OK ] UnitTestsFixture.test0 (0 ms)
[ RUN      ] UnitTestsFixture.test1
UnitTests2.cpp:29: Failure
Expected equality of these values:
  cycle_length( 5)
    Which is: 6
  5
[  FAILED  ] UnitTestsFixture.test1 (0 ms)
[ RUN      ] UnitTestsFixture.test2
[       OK ] UnitTestsFixture.test2 (0 ms)
[----------] 3 tests from UnitTestsFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 3 tests from 1 test suite ran. (0 ms total)
[  PASSED  ] 2 tests.
[  FAILED  ] 1 test, listed below:
[  FAILED  ] UnitTestsFixture.test1

 1 FAILED TEST
*/





// --------------
// UnitTests3.cpp
// --------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(UnitTestsFixture, test0) {
    ASSERT_EQ(cycle_length( 1), 1);}

TEST(UnitTestsFixture, test1) {
    ASSERT_EQ(cycle_length( 5), 6);}

TEST(UnitTestsFixture, test2) {
    ASSERT_EQ(cycle_length(10), 7);}

/*
% ./UnitTests3
Running main() from /tmp/googletest-20230802-5079-wamwr8/googletest-1.14.0/googletest/src/gtest_main.cc
[==========] Running 3 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 3 tests from UnitTestsFixture
[ RUN      ] UnitTestsFixture.test0
[       OK ] UnitTestsFixture.test0 (0 ms)
[ RUN      ] UnitTestsFixture.test1
[       OK ] UnitTestsFixture.test1 (0 ms)
[ RUN      ] UnitTestsFixture.test2
[       OK ] UnitTestsFixture.test2 (0 ms)
[----------] 3 tests from UnitTestsFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 3 tests from 1 test suite ran. (0 ms total)
[  PASSED  ] 3 tests.
*/





// -------------
// Coverage1.cpp
// -------------

// https://gcc.gnu.org/onlinedocs/gcc/Gcov.html

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(CoverageFixture, test) {
    ASSERT_EQ(cycle_length(1), 1);}

/*
% clang++ --coverage -g -std=c++17 -Wall -Wextra -Wpedantic Coverage1.cpp -o Coverage1



% ./Coverage1
Running main() from /tmp/googletest-20210612-68642-1k2ka09/googletest-release-1.12.1/googletest/src/gtest_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from CoverageFixture
[ RUN      ] CoverageFixture.test
[       OK ] CoverageFixture.test (0 ms)
[----------] 1 test from CoverageFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.



% ls -al Coverage1.*
-rw-r--r--@ 1 downing  staff    1558 Sep  4 14:44 Coverage1.cpp
-rw-r--r--@ 1 downing  staff    2678 Mar  1 16:09 Coverage1.cpp.gcov
-rw-r--r--@ 1 downing  staff    8764 Mar  1 16:09 Coverage1.gcda
-rw-r--r--@ 1 downing  staff  131168 Mar  1 16:09 Coverage1.gcno



% llvm-cov gcov Coverage1.cpp | grep -B 2 "cpp.gcov"
File 'Coverage1.cpp'
Lines executed:66.67% of 12
Creating 'Coverage1.cpp.gcov'
*/





// -------------
// Coverage2.cpp
// -------------

// https://gcc.gnu.org/onlinedocs/gcc/Gcov.html

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(CoverageFixture, test) {
    ASSERT_EQ(cycle_length(2), 2);}

/*
% ./Coverage2
Running main() from /tmp/googletest-20210612-68642-1k2ka09/googletest-release-1.12.1/googletest/src/gtest_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from CoverageFixture
[ RUN      ] CoverageFixture.test
[       OK ] CoverageFixture.test (0 ms)
[----------] 1 test from CoverageFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.



% llvm-cov gcov Coverage2.cpp | grep -B 2 "cpp.gcov"
File 'Coverage2.cpp'
Lines executed:91.67% of 12
Creating 'Coverage2.cpp.gcov'
*/





// -------------
// Coverage3.cpp
// -------------

// https://gcc.gnu.org/onlinedocs/gcc/Gcov.html

#include <cassert>  // assert
#include <iostream> // cout, endl

#include "gtest/gtest.h"

int cycle_length (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0)
            n /= 2;
        else
            ++(n *= 3);
        ++c;}
    assert(c > 0);
    return c;}

TEST(CoverageFixture, test) {
    ASSERT_EQ(cycle_length(3), 8);}

/*
% ./Coverage3
Running main() from /tmp/googletest-20210612-68642-1k2ka09/googletest-release-1.12.1/googletest/src/gtest_main.cc
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from CoverageFixture
[ RUN      ] CoverageFixture.test
[       OK ] CoverageFixture.test (0 ms)
[----------] 1 test from CoverageFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.



% llvm-cov gcov Coverage3.cpp | grep -B 2 "cpp.gcov"
File 'Coverage3.cpp'
Lines executed:100.00% of 12
Creating 'Coverage3.cpp.gcov'
*/
