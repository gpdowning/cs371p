// -----------
// Fri, 28 Feb
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 3 Mar: Blog  #7
    Sun, 3 Mar: Paper #7

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 28 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 3 Mar, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 12 Mar: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #4: WiCS
    https://www.cs.utexas.edu/~utpc/
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/





/*
last time
    allocator

this time
*/





// -------------
// Allocator.cpp
// -------------

template <typename T>
void my_construct (T* p, const T& v) {
    new (p) T(v);}

template <typename T>
void my_destroy (T* p) {
    p->~T();}

using array_t = array<size_t, 5>;

class A {
    friend bool operator == (const A& lhs, const A& rhs) {
        return lhs._v == rhs._v;}

    friend bool operator < (const A& lhs, const A& rhs) {
        return lhs._v < rhs._v;}

    public:
        static array_t c;

        static void reset () {
            c = array_t({0, 0, 0, 0, 0});}

    private:
        int _v;

    public:
        A () :                         // default constructor
            _v (0) {
            ++c[0];}

        A (int v) :                    // int constructor
            _v (v) {
            ++c[1];}

        A (const A& rhs) :             // copy constructor
            _v (rhs._v) {
            ++c[2];}

        A& operator = (const A& rhs) { // copy assignment
            _v = rhs._v;
            ++c[3];
            return *this;}

        ~A () {                        // destructor
            ++c[4];}};

array_t A::c;

void test1 () {
    A::reset();
    allocator<A> al;
    assert(A::c == array_t({0, 0, 0, 0, 0}));

    const size_t s = 1;

    A::reset();
    A* a = al.allocate(s);
    assert(A::c == array_t({0, 0, 0, 0, 0}));

    A::reset();
    const A v = 2;
    assert(A::c == array_t({0, 1, 0, 0, 0})); // A(int)

    A::reset();
    my_construct(a, v);                       // A(const A&)
    assert(A::c == array_t({0, 0, 1, 0, 0}));

    assert(*a == v);

    A::reset();
    my_destroy(a);                            // ~A()
    assert(A::c == array_t({0, 0, 0, 0, 1}));

    A::reset();
    al.deallocate(a, s);
    assert(A::c == array_t({0, 0, 0, 0, 0}));}

void test2 () {
    A::reset();
    allocator<A> al;
    assert(A::c == array_t({0, 0, 0, 0, 0}));

    const size_t s = 3;

    A::reset();
    A* b = al.allocate(s);
    A* e = b + s;
    assert(A::c == array_t({0, 0, 0, 0, 0}));

    A::reset();
    const A v = 2;                            // A(int)
    assert(A::c == array_t({0, 1, 0, 0, 0}));

    A::reset();
    uninitialized_fill(b, e, v);
    assert(A::c == array_t({0, 0, 3, 0, 0})); // 3 A(const A&)

    assert(equal(b, e, begin({2, 2, 2})));

    A::reset();
    destroy(b, e);
    assert(A::c == array_t({0, 0, 0, 0, 3})); // 3 ~A()

    A::reset();
    al.deallocate(b, s);
    assert(A::c == array_t({0, 0, 0, 0, 0}));}





template <typename T, typename AL = allocator<T>>
class vector {
    private:
        AL _al;

        T* _b;
        T* _e;

    pubilc:
        ...};





uninitialized_fill(b, e, v)
uninitialized_copy(b1, e1, b2)
destroy(b, e)





// -----------
// Vector1.cpp
// -----------

template <typename T, typename AL = allocator<T>>
class my_vector {
    friend bool operator == (const my_vector& lhs, const my_vector& rhs) {
        return (lhs.size() == rhs.size()) && equal(lhs.begin(), lhs.end(), rhs.begin());}

    friend bool operator != (const my_vector& lhs, const my_vector& rhs) {
        return !(lhs == rhs);}

    private:
        AL _al;

        T* _b = nullptr;
        T* _e = nullptr;

    public:
        my_vector () = default;

        explicit my_vector (size_t s) :
                my_vector(s, T())
            {}

        my_vector (size_t s, const T& v) {
            if (s != 0) {
                _b = _al.allocate(s);
                _e = _b + s;
                uninitialized_fill(begin(), end(), v);}}

        my_vector (initializer_list<T> rhs) {
            if (rhs.size() != 0) {
                _b = _al.allocate(rhs.size());
                _e = _b + rhs.size();
                uninitialized_copy(rhs.begin(), rhs.end(), begin());}}

        my_vector             (const my_vector&) = delete;
        my_vector& operator = (const my_vector&) = delete;

        ~my_vector () {
            destroy(_b, _e);
            _al.deallocate(_b, size());}

        T& operator [] (size_t i) {
            assert(i < size());
            return _b[i];}

        const T& operator [] (size_t i) const {
            return (*const_cast<my_vector*>(this))[i];}

        T* begin () {
            return _b;}

        const T* begin () const {
            return const_cast<my_vector*>(this)->begin();}

        T* end () {
            return _e;}

        const T* end () const {
            return const_cast<my_vector*>(this)->end();}

        size_t size () const {
            return _e - _b;}};
