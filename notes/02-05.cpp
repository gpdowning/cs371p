// -----------
// Wed,  5 Feb
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 9 Feb: Blog  #4
    Sun, 9 Feb: Paper #4

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri,  7 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Wed, 12 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/

    Mon, 10 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/
*/





/*
last time
    iteration

this time
    Project #2: Voting
*/





/*
. have to partner for at least one of the 4 remaining projects
. if you partner more than once, it has to be a different partner
. join the Canvas group
*/





/*
1. issues by adapting the old Grades.csv
2. no starter code
3. your own Makefile
4. your own gitlab-ci.yml
5. your own Voting.ctd.txt (crowd source that on Ed)
*/





// sample input

3 // number of test cases

3 // number of candidates
John Doe
Jane Smith
Sirhan Sirhan
1 2 3
2 1 3         // position of the number is about the preferenc, left to right
1 3 2         // value    of the number is the index of the candidates
1 2 3

3
John Doe
Jane Smith
Sirhan Sirhan
1 2 3
2 1 3
2 3 1
1 2 3

3
John Doe
Jane Smith
Sirhan Sirhan
1 2 3
2 1 3
2 3 1
1 2 3
3 1 2

// sample output

John Doe // more then 50% of the ballots in the 1st round

John Doe   // tie
Jane Smith

John Doe // more than 50% of the ballots in the 2nd round





/*
1. majority, more than half, makes you a winner
2. if there's tie, we're done, print all of the tied candidates
*/





// max of 20 candidates
// max of 1000 ballots





/*
1st solution
vector of ballots
vector of candidates
*/

/*
2nd solution
vector of ballots per candidate
vector of candidates
*/

/*
3rd solution
vector of ballots
vector of ballot pointers per candidate
vector of candidates
*/





// always best to do the simplest implementation first!!!





vector<int> x(10, 2); // size is 10, capacity is 10
int* p = &x[0];
cout << *p;           // 2
x.push_back(3);       // O(n), size is 11, capacity is 20
cout << *p;           // UNDEFINED





vector<int> x(10, 2); // size and capacity are both 10
x.reserve(20);        // O(n), size is 10, capacity is 20
int* p = &x[0];
cout << *p;           // 2
x.push_back(3);       // O(1), size becomes 11, capacity becomes 20
cout << *p;           // 2, DEFINED
