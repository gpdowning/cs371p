// -----------
// Mon, 10 Feb
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 9 Feb: Blog  #5
    Sun, 9 Feb: Paper #5

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Mon, 10 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 12 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/

    Fri, 14 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/
*/





/*
last time
    Exercise #3: reverse()

this time
    iteration
    range
*/





// -------------
// Iteration.cpp
// -------------

void test5 () {
    int a[] = {2, 3, 4};
    int s   = 0;
    for (int v : a)
        s += v;
    assert(s == 9);
    assert(equal(a, a + 3, begin({2, 3, 4})));}

void test6 () {
    int  a[] = {2, 3, 4};
    int  s   = 0;
    int* b   = begin(a);
    int* e   = end(a);
    while (b != e) {
        int v = *b;
        s += v;
        ++b;}
    assert(s == 9);
    assert(equal(a, a + 3, begin({2, 3, 4})));}

void test7 () {
    int a[] = {2, 3, 4};
    int s   = 0;
    for (int v : a) {
        s += v;
        ++v;}                                   // ?
    assert(s == 9);
    assert(equal(a, a + 3, begin({2, 3, 4})));}

void test8 () {
    int a[] = {2, 3, 4};
    int s   = 0;
    for (auto v : a) {
        s += v;
        ++v;}                                   // ?
    assert(s == 9);
    assert(equal(a, a + 3, begin({2, 3, 4})));
    auto [vx, vy, vz] = a;
    assert(vx  == 2);
    assert(vy  == 3);
    assert(vz  == 4);
    assert(&vx != &a[0]);}

void test9 () {
    int a[] = {2, 3, 4};
    int s   = 0;
    for (int& r : a) {
        s += r;
        ++r;}
    assert(s == 9);
    assert(equal(a, a + 3, begin({3, 4, 5})));}

void test10 () {
    int a[] = {2, 3, 4};
    int s   = 0;
    for (auto& r : a) {
        s += r;
        ++r;}
    assert(s == 9);
    assert(equal(a, a + 3, begin({3, 4, 5})));
    auto& [rx, ry, rz] = a;
    assert(rx  == 3);
    assert(ry  == 4);
    assert(rz  == 5);
    assert(&rx == &a[0]);}





/*
input iterator
    * (read only), ++, ==, !=

output iterator
    * (write only), ++

forward iterator
    * (read write), ++, ==, !=

bidirectional iterator
    * (read write), ++, ==, !=, --

random access iterator
    * (read write), ++, ==, !=, --, <, <=, >, >=, [], +/- int, -
*/





/*
forward_list
    singly linked list
    forward
*/

forward_list<...> fl(...);
fill(begin(fl), end(fl), v)

reverse(begin(fl), end(fl)) // no





/*
list
    doubly linked list
    bidirectional
*/

list<...> l(...);
reverse(begin(l), end(l))
sort(begin(l), end(l))     // no





/*
vector, deque
    arrays
    random_access iterator
*/

vector<...> v(...);
sort(begin(v), end(v))





void test11 () {
    list<int> x = {2, 3, 4};
    int       s = 0;
    for (int v : x)
        s += v;
    assert(s == 9);
    assert(equal(begin(x), end(x), begin({2, 3, 4})));}

void test12 () {
    list<int>           x = {2, 3, 4};
    int                 s = 0;
    list<int>::iterator b = begin(x);                   // x.begin()
    list<int>::iterator e = end(x);                     // x.end()
    while (b != e) {
        list<int>::iterator::value_type v = *b;
        s += v;
        ++b;}
    assert(s == 9);
    assert(equal(begin(x), end(x), begin({2, 3, 4})));}





/*
set           is like Java's TreeSet, needs comparison
unordered_set is like Java's HashSet, needs hashable keys
    no duplicates
    mutable structure, immutable keys
*/

void test13 () {
    set<int> x = {2, 3, 4};
    int      s = 0;
    for (int v : x)
        s += v;
    assert(s == 9);
    assert(equal(begin(x), end(x), begin({2, 3, 4})));}

void test14 () {
    set<int>           x = {2, 3, 4};
    int                s = 0;
    set<int>::iterator b = begin(x);           // x.begin()
    set<int>::iterator e = end(x);             // x.end()
    while (b != e) {
        set<int>::iterator::value_type v = *b;
        s += v;
        ++b;}
    assert(s == 9);
    assert(equal(begin(x), end(x), begin({2, 3, 4})));}

void test15 () {
    set<int> x = {2, 3, 4};
    int      s = 0;
//  for (      int& r : x)                              // error: binding reference of type 'int&' to 'const int' discards qualifiers
    for (const int& r : x)
        s += r;
    assert(s == 9);
    assert(equal(begin(x), end(x), begin({2, 3, 4})));}





/*
map           is like Java's TreeMap
unordered_map is like Java's HashMap
    no duplicate keys
    mutable structure, immutable keys, mutable values
*/

void test16 () {
    map<char, int> x = {{'a', 2}, {'b', 3}, {'c', 4}};
    int            s = 0;
    for (pair<char, int> v : x)
        s += v.second;
    assert(s == 9);}

void test17 () {
    map<char, int>           x = {{'a', 2}, {'b', 3}, {'c', 4}};
    int                      s = 0;
    map<char, int>::iterator b = begin(x);                       // x.begin()
    map<char, int>::iterator e = end(x);                         // x.end()
    while (b != e) {
        map<char, int>::iterator::value_type v = *b;
        s += v.second;
        ++b;}
    assert(s == 9);}

void test18 () {
    map<char, int> x = {{'a', 2}, {'b', 3}, {'c', 4}};
    int            s = 0;
//  for (pair<      char, int>& p : x)                 // error: non-const lvalue reference to type 'pair<char, [...]>' cannot bind to a value of unrelated type 'const pair<__key_type, [...]>'
    for (pair<const char, int>& p : x)
        s += p.second;
    assert(s == 9);}

void test19 () {
    ostringstream     out;
    const vector<int> x = {2, 3, 4};
    for (size_t i = 0; i != x.size(); ++i)
        out << x[i] << " \n"[i == (x.size() - 1)];
    assert(out.str() == "2 3 4\n");}





range x(10);      // 0..9

range y(10, 20);  // 10..19

range z(2, 5, 2); // 2, 4





range::iterator b = begin(z); // z.begin()
range::iterator e = end(z);   // z.end()

b == e // b.operator==(e)

++b;   // b.operator++()
b++;   // b.operator++(0)

*b     // b.operator*()





class range {
    class iterator {
        itrator (...) {
            ...}

        bool operator == (const iterator& rhs) const {
            ...}

        bool operator != (const iterator& rhs) const {
            return !(*this == rhs);}

        int operator * () const {
            ...}

        iterator& operator ++ () {  // pre-increment
            ...}

        iterator operator ++ (int) { // post-increment
            ...}};

    range (...) {
        ...}

    bool operator == (const range& rhs) const {
        ...}

    bool operator != (const range& rhs) const {
        return !(*this == rhs);}

    int operator [] (int index) const {
        ...}

    iterator begin () const {
        return iterator(...);}

    iterator end () const {
        return iterator(...);}

    int size () const {
        ...}};
