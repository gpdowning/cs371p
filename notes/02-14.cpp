// -----------
// Fri, 14 Feb
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 9 Feb: Blog  #5
    Sun, 9 Feb: Paper #5

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Wed, 12 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/

    Fri, 14 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 17 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/





/*
last time
    const methods
    Exercise #4: range

this time
    arrays
*/





// -----------
// Arrays1.cpp
// -----------

void test1 () {
    int a[] = {2, 3, 4}; // int* const a; O(n)
    assert(*a == a[0]);
    assert(a  == &a[0]);
    assert(sizeof(a)     != sizeof(&a[0]));
    assert(sizeof(a)     == 12);
    assert(sizeof(&a[0]) == 8);
//  ++a;                                    // error: lvalue required as left operand of assignment
    ++a[1];
    ++*(a + 1);
    assert(a[1]     == 5);
    assert(*(a + 1) == 5);
//  assert(a[3]     == 0);                  // undefined
//  assert(*(a + 3) == 0);                  // undefined
    }

T a[] = {2, 3, 4}; // T(int), 3 times

void test2 () {
    const size_t s    = 10;
    const int    a[s] = {2, 3, 4}; // O(n)
    assert(sizeof(a) == 40);
    assert(a[1]      ==  3);
    assert(*(a + 1)  ==  3);
    assert(a[s - 1]  ==  0);
//  ++a;                           // error: lvalue required as left operand of assignment
//  ++a[1];                        // error: increment of read-only location
    }





T a[10] = {2, 3, 4}; // T(int), 3 times; T(), 7 times

class A {};

A x;





class A {
    A (int) {...}};

A x(2);
A x;    // no





class A {
    A () {...}
    A (int) {...}};

A x(2);
A x;





class A {
    A () = default;
    A (int) {...}};

A x(2);
A x;





void test3 () {
    const size_t s = 10;
//  const int    a[s];       // error: uninitialized const 'a'
    int a[s];                // O(1)
    assert(sizeof(a) == 40);
//  assert(a[0]      ==  0); // undefined
    }

T a[10]; // T(), 10 times; O(n)

void test4 () {
    const size_t s    = 10;
    const int    a[s] = {}; // O(n)
    assert(sizeof(a) == 40);
    assert(a[0]      ==  0);}

T a[10] = {}; // T(), 10 times

void test5 () {
    int a[] = {2, 3, 4};
    int b[] = {2, 3, 4};
    assert(a != b);
    assert(equal(a, a + 3, b));
    ++b[1];
    assert(equal(a, a + 3, begin({2, 3, 4})));
    assert(equal(b, b + 3, begin({2, 4, 4})));}

void test6 () {
    int a[] = {2, 3, 4};
//  int b[] = a;                    // error: initializer fails to determine size of 'b'
    int* b  = a;
    assert(a         == b);
    assert(sizeof(a) != sizeof(b));
    assert(sizeof(a) == 12);
    assert(sizeof(b) ==  8);
    ++b[1];
    assert(equal(a, a + 3, begin({2, 4, 4})));
    assert(equal(b, b + 3, begin({2, 4, 4})));}

void test7 () {
    int          a[] = {2, 3, 4};
    const size_t s   = sizeof(a) / sizeof(a[0]);
    int b[s]; // O(1)
    copy(a, a + s, b); // O(n)
    assert(a != b);
    assert(equal(a, a + s, b));
    ++b[1];
    assert(equal(a, a + 3, begin({2, 3, 4})));
    assert(equal(b, b + 3, begin({2, 4, 4})));}





template <typename II, typename OI>
OI my_copy (II b1, II e1, OI b2) {
    while (b1 != e1) {
        *b2 = *b1;
        ++b1;
        ++b2;}
    return b2;}

T b[s];            // O(n): T(),  s times
copy(a, a + s, b); // O(n): =(T), s times





void test8 () {
    int a[] = {2, 3, 4};
    int b[] = {5, 6, 7};
//  b = a;                                     // error: invalid array assignment
    const size_t s = sizeof(a) / sizeof(a[0]);
    copy(a, a + s, b);
    assert(a != b);
    assert(equal(a, a + s, b));
    ++b[1];
    assert(equal(a, a + 3, begin({2, 3, 4})));
    assert(equal(b, b + 3, begin({2, 4, 4})));}

void f (int p[]) {
    assert(sizeof(p) == 8); // warning: sizeof on array function parameter will return size of 'int *' instead of 'int []'
    ++p;
    ++p[0];
    ++*p;}

void test9 () {
    int a[] = {2, 3, 4};
    f(a);
    assert(equal(a, a + 3, begin({2, 5, 4})));}

void g (int* p) {
    assert(sizeof(p) == 8);
    ++p;
    ++p[0];
    ++*p;}

void test10 () {
    int a[] = {2, 3, 4};
    g(a);
    assert(equal(a, a + 3, begin({2, 5, 4})));}





array<int, 3> x(...);
array<int, 3> y(...); // same type as x
array<int, 4> z(...); // different type than x or y





void test11 () {
    array<int, 3> x = {2, 3, 4}; // O(n)
    assert(x.size() == 3);
    assert(x[1]     == 3);
    array<int, 3> y(x);
    assert( x    ==  y);
    assert(&x[1] != &y[1]);}

array<T, 3> x = {2, 3, 4}; // O(n), T(int), 3 times

void test12 () {
    array<int, 3> x = {2, 3, 4};
    array<int, 3> y = {5, 6, 7};
    x = y;
    assert( x    ==  y);
    assert(&x[1] != &y[1]);}

void h1 (array<int, 3>& y) {
    array<int, 3>::iterator p = begin(y);
    ++p;
    ++p[0];
    ++*p;}

void test13 () {
    array<int, 3> x = {2, 3, 4};
    h1(x);
    assert(equal(begin(x), end(x), begin({2, 5, 4})));}

void h2 (array<int, 3> y) {
    array<int, 3>::iterator p = begin(y);
    ++p;
    ++p[0];
    ++*p;}

void test14 () {
    array<int, 3> x = {2, 3, 4};
    h2(x);
    assert(equal(begin(x), end(x), begin({2, 3, 4})));}
