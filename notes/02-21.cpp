// -----------
// Fri, 21 Feb
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 9 Feb: Blog  #5
    Sun, 9 Feb: Paper #5

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 21 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 24 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 26 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #3
    https://www.cs.utexas.edu/~utpc/
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/





/*
last time
    initializations
    std::initializer_list

this time
    Exercise #5: Array
*/





template <typename T, size_t N>
class my_array {
    friend bool operator == (const my_array& lhs, const my_array& rhs) {
        return equal(lhs.begin(), lhs.end(), rhs.begin());}

    friend bool operator != (const my_array& lhs, const my_array& rhs) {
        return !(lhs == rhs);}

private:
    T _a[N];

public:
    // my_array             (const my_array&) = default; // copy    constructor
    // my_array& operator = (const my_array&) = default; // copy    assignment
    // ~my_array            ()                = default; // destructor

    my_array () = default;

    my_array (initializer_list<T> il) {
        copy(il.begin(), il.end(), _a);}

    T& operator [] (size_t i) {
        return _a[i];}

    const T& operator [] (size_t i) const {
        return const_cast<my_array&>(*this)[i];}

    T& at (size_t i) {
        if (i >= size())
            throw out_of_range("Array::at index out of range");
        return (*this)[i];}

    const T& at (size_t i) const {
        return const_cast<my_array&>(*this).at(i);}

    T* begin () {
        return _a;}

    const T* begin () const {
        return const_cast<my_array&>(*this).begin();}

    T* end () {
        return _a + N;}

    const T* end () const {
        return const_cast<my_array&>(*this).end();}

    void fill (const T& v) {
        std::fill(begin(), end(), v);}

    size_t size () const {
        return N;}};





template <typename T, size_t N>
struct my_array {
    friend bool operator == (const my_array& lhs, const my_array& rhs) {
        return equal(lhs.begin(), lhs.end(), rhs.begin());}

    friend bool operator != (const my_array& lhs, const my_array& rhs) {
        return !(lhs == rhs);}

    T _a[N];

    // my_array             (const my_array&) = default; // copy    constructor
    // my_array& operator = (const my_array&) = default; // copy    assignment
    // ~my_array            ()                = default; // destructor

    T& operator [] (size_t i) {
        return _a[i];}

    const T& operator [] (size_t i) const {
        return const_cast<my_array&>(*this)[i];}

    T& at (size_t i) {
        if (i >= size())
            throw out_of_range("Array::at index out of range");
        return (*this)[i];}

    const T& at (size_t i) const {
        return const_cast<my_array&>(*this).at(i);}

    T* begin () {
        return _a;}

    const T* begin () const {
        return const_cast<my_array&>(*this).begin();}

    T* end () {
        return _a + N;}

    const T* end () const {
        return const_cast<my_array&>(*this).end();}

    void fill (const T& v) {
        std::fill(begin(), end(), v);}

    size_t size () const {
        return N;}};

