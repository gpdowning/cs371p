// -----------
// Fri, 24 Jan
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 26 Jan: Blog  #2
    Sun, 26 Jan: Paper #2: Makefile

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 24 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 27 Jan, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/
*/





int i = 2;
int j = ++i;
cout << i << "  " << j; // 3 3

++++i;



int i = 2;
int j = i++;
cout << i << "  " << j; // 3 2

i++++; // no

++i++; // no





for (int i = 0; i != s; i++) // gets rewritten to use ++i
    ...

for (I i = 0; i != s; i++) // can't be rewritten, and will be more expensive
    ...





/*
1. run as is, confirm success
2. identify, remember, fix the broken test
3. run it again, confirm failure
4. identify, remember, fix the broken code
5. run as is, confirm success
*/

bool is_prime (int n) {
    assert(n > 0);
    if (n == 2)
        return true;
    if ((n == 1) || ((n % 2) == 0))
        return false;
    for (int i = 3; i < (std::sqrt(n) + 1); ++++i)
        if ((n % i) == 0)
            return false;
    return true;}





/*
last time
    assertions
    unit tests

this time
    exercise
    exceptions
*/





// C doesn't have exceptions

// use the return

int f (...) {
    ...
    if (<something wrong>)
        return <special value>
    ...}

void g (...) {
    ...
    int x = f(...);
    if (x = <special value)
        <something wrong>
    ...}





// use a global

int h = 0;

int f (...) {
    ...
    if (<something wrong>) {
        h = <special value>
        return ...}
    ...}

void g (...) {
    ...
    int h = 0;
    int x = f(...);
    if (h = <special value)
        <something wrong>
    ...}





// use an arg, by value, doesn't work

int f (..., int e2) {
    ...
    if (<something wrong>) {
        e2 = <special value>
        return ...}
    ...}

void g (...) {
    ...
    int e = 0;
    int x = f(..., e);
    if (e = <special value)
        <something wrong>
    ...}





// use an arg, by address, does work

int f (..., int* p) {
    ...
    if (<something wrong>) {
        *p = <special value>
        return ...}
    ...}

void g (...) {
    ...
    int e = 0;
    int x = f(..., &e);
    if (e = <special value)
        <something wrong>
    ...}





// use an arg, by reference, does work

int f (..., int& r) {
    ...
    if (<something wrong>) {
        r = <special value>
        return ...}
    ...}

void g (...) {
    ...
    int e = 0;
    int x = f(..., e);
    if (e = <special value)
        <something wrong>
    ...}
