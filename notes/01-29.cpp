// -----------
// Wed, 29 Jan
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 2 Feb: Blog  #3
    Sun, 2 Feb: Paper #3

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Wed, 29 Jan: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #1
    https://www.cs.utexas.edu/~utpc/

    Fri, 31 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 3 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/
*/






/*
last time
    values
    addresses
    references
    call by value
    call by address
    call by reference
    exceptions
    catch by reference

this time
    exceptions
    Exercise #2: strcmp()
*/





// --------------
// Exceptions.cpp
// --------------

// https://en.cppreference.com/w/cpp/language/exceptions

#include <cassert>   // assert
#include <cstring>   // strcmp
#include <iostream>  // cout, endl
#include <stdexcept> // domain_error
#include <string>    // string

using namespace std;

int f (bool b) {
    if (b)
        throw domain_error("abc");
    return 0;}

void test1 () {
    try {
        assert(f(false) == 0);
        }
    catch (domain_error& e) {
        assert(false);}}

void test2 () {
    try {
        f(true);
        assert(false);
        }
    catch (domain_error& e) {
//      assert(       e                == "abc");   // error: no match for ‘operator==’ in ‘e == "abc"’
//      assert(       e.what()         != "abc");   // warning: comparison with string literal results in unspecified behavior
        assert(strcmp(e.what(), "abc") == 0);
        assert(string(e.what())        == "abc");}}

int strcmp (const char* a, const char* b) {
    ...}

void test3 () {
    domain_error x("abc");
    logic_error& y = x;
    exception&   z = y;
    assert(&x == &z);}

int main () {
    cout << "Exceptions.cpp" << endl;
    test1();
    test2();
    test3();
    cout << "Done." << endl;
    return 0;}





// -----------
// StrCmpT.cpp
// -----------

// https://en.cppreference.com/w/c/string/byte/strcmp
// https://gcc.gnu.org/onlinedocs/gcc-10.2.0/libstdc++/api/a00332_source.html

#include <cstring>    // strcmp
#include <functional> // function

#include "gtest/gtest.h"

int my_strcmp (const char* a, const char* b) {
    while (*a && (*a == *b)) {
        ++a;
        ++b;}
    return (*a - *b);}

using namespace std;
using namespace testing;

using StrCmpSignature = function<int (const char*, const char*)>;

struct StrCmpFixture : TestWithParam<StrCmpSignature> {};

INSTANTIATE_TEST_SUITE_P(
    StrCmpInstantiation,
    StrCmpFixture,
    Values(
           strcmp,
        my_strcmp));

TEST_P(StrCmpFixture, test1) {
    ASSERT_EQ(GetParam()("", ""), 0);}

TEST_P(StrCmpFixture, test2) {
    ASSERT_EQ(GetParam()("abc", "abc"), 0);}

TEST_P(StrCmpFixture, test3) {
    ASSERT_GT(GetParam()("abc", "ab"), 0);}

TEST_P(StrCmpFixture, test4) {
    ASSERT_GT(GetParam()("abc", "aba"), 0);}

TEST_P(StrCmpFixture, test5) {
    ASSERT_LT(GetParam()("ab", "abc"), 0);}

TEST_P(StrCmpFixture, test6) {
    ASSERT_LT(GetParam()("aba", "abc"), 0);}
