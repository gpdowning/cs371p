// -----------
// Wed, 12 Feb
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 9 Feb: Blog  #5
    Sun, 9 Feb: Paper #5

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Wed, 12 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/

    Fri, 14 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 17 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/
*/

/*
SOLID design principle
1. The Single Responsibility Principle
2. The Open-Closed Principle
3. The Liskov Substitution Principle
4. The Interface Segregation Principle
5. The Dependency Inversion Principle
*/





/*
last time
    iteration
    range

this time
    const methods
    Exercise #4: range
*/





template <typename T>
class vector {
    private:
        ...
    public:
        T operator [] (int i) {
            ...}

vector<int> x(10, 2);
cout << x[1];         // 2
x[1] = 3;             // no





template <typename T>
class vector {
    private:
        ...
    public:
        T& operator [] (int i) {
            ...}

vector<int> x(10, 2);
cout << x[1];         // 2
x[1] = 3;

const vector<int> y(10, 2);
cout << y[1];               // no





template <typename T>
class vector {
    private:
        ...
    public:
        T& operator [] (int i) const {
            ...}

vector<int> x(10, 2);
cout << x[1];         // 2
x[1] = 3;

const vector<int> y(10, 2);
cout << y[1];
y[1] = 3;                   // yes!!!





template <typename T>
class vector {
    private:
        ...
    public:
        const T& operator [] (int i) const {
            ...}

vector<int> x(10, 2);
cout << x[1];         // 2
x[1] = 3;             // no

const vector<int> y(10, 2);
cout << y[1];               // 2
y[1] = 3;                   // no





template <typename T>
class vector {
    private:
        ...
    public:
        T& operator [] (int i) {
            ...}

        const T& operator [] (int i) const {
            ...}

vector<int> x(10, 2);
cout << x[1];         // 2
x[1] = 3;

const vector<int> y(10, 2);
cout << y[1];               // 2
y[1] = 3;





emplate <typename T>
class range {
    public:
        class iterator {
            private:
                T _v;

            public:
                iterator (const T& v) {
                    _v = v;}

                bool operator == (const iterator& rhs) const {
                    return (_v == rhs._v);}

                bool operator != (const iterator& rhs) const {
                    return !(*this == rhs);}

                T operator * () const {
                    return _v;}

                iterator& operator ++ () {
                    ++_v;
                    return *this;}

                iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}};

    private:
        T _start;
        T _stop;

    public:
        range (const T& start, const T& stop) {
            _start = start;
            _stop  = stop;}

        bool operator == (const range& rhs) const {
            return (_start == rhs._start) && (_stop == rhs._stop);}

        bool operator != (const range& rhs) const {
            return !(*this == rhs);}

        T operator [] (int index) const {
            if ((index < 0) || (index >= (_stop - _start)))
                throw out_of_range("range");
            return _start + index;}

        iterator begin () const {
            return iterator(_start);}

        iterator end () const {
            return iterator(_stop);}

        int size () const {
            return _stop - _start;}};
