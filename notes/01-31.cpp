// -----------
// Fri, 31 Jan
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 2 Feb: Blog  #3
    Sun, 2 Feb: Paper #3

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Fri, 31 Jan, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Mon, 3 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Wed, 12 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/
*/





/*
last time
    exceptions
    Exercise #2: strcmp()

this time
    consts
*/





const int i;     // no, must initialize
const int i = 2;
++i;             // no





// many-location, read-write pointer

int       i  = 2;
const int ci = 3;
int* p;            // yes
p = &i;
p = &ci;           // no
++*p;              // yes





// many-location, read-only pointer

int        i  = 2;
const int  ci = 3;
const int* pc;     // yes
pc = &i;
pc = &ci;
++*p;              // no





// one-location, read-write pointer

int        i  = 2;
const int  ci = 3;
int* const cp;       // no, must initialize
int* const cp = &i;
++*cp;
cout << i;           // 3
int* const cq = &ci; // no





int* p = new int[100];
++*p;
delete [] p;

int* p = new int[100];
++p;
delete [] p; // no

int* const p = new int[100];
++p;                         // no
delete [] p;





/*
delete a bad address
forget to delete
delete a good address more than once
continue to use the data pointed at after deleting
*/





// one-location, read-only pointer

int              i  = 2;
const int        ci = 3;
const int* const cpc;       // no
const int* const cpc = &i;
const int* const cqc = &ci;
++cpc;
++*cpc;





// ----------
// Consts.cpp
// ----------

void test1 () {
    int i = 2;
    ++i;
    assert(i == 3);}

void test2 () {
//  const int ci;     // error: uninitialized const 'ci'
    const int ci = 4;
//  ++ci;             // error: increment of read-only variable 'ci'
    assert(ci == 4);}

void test3 () {
    // read/write, many-location pointer
    // mutable int, mutable pointer
    int       i  = 2;
    const int ci = 3;
    int*      p;
    p = &i;
    ++*p;
    assert(i == 3);
//  p = &ci;        // error: invalid conversion from 'const int*' to 'int*'
    assert(ci);}

void test4 () {
    // read-only, many-location pointer
    // immutable int, mutable pointer
    int        i  = 2;
    const int  ci = 3;
    const int* pc;
    pc = &ci;
//  ++*pc;                         // error: increment of read-only location
//  int* p = pc;                   // error: invalid conversion from 'const int*' to 'int*'
    int* p = const_cast<int*>(pc);
    assert(p == pc);
//  ++*p;                          // undefined
    pc = &i;                       // ?
    p = const_cast<int*>(pc);
    ++*p;
    assert(i == 3);}

void test5 () {
    // read/write, one-location pointer
    // mutable int, immutable pointer
    int        i  = 2;
    const int  ci = 3;
//  int* const cp;       // error: uninitialized const 'cp'
//  int* const cp = &ci; // error: invalid conversion from 'const int*' to 'int*'
    int* const cp = &i;
//  ++cp;                // error: cannot assign to variable 'cp' with const-qualified type 'int *const'
    ++*cp;
    assert(i == 3);
    assert(ci);}

void test6 () {
    // read-only, one-location pointer
    // immutable int, immutable pointer
    int              i   = 2;
    const int        ci  = 3;
//  const int* const cpc;       // error: uninitialized const 'cpc'
    const int* const cpc = &ci;
    const int* const cqc = &i;
//  ++cqc;                      // error: cannot assign to variable 'cqc' with const-qualified type 'const int *const'
//  ++*cqc;                     // error: increment of read-only location
    assert(cpc);
    assert(cqc);}

void test7 () {
    // read/write reference
    // mutable int
    int       i  = 2;
    const int ci = 3;
//  int&      r;      // error: 'r' declared as reference but not initialized
//  int&      r = ci; // error: invalid initialization of reference of type 'int&' from expression of type 'const int'
    int&      r = i;
    ++r;
    assert(i == 3);
    assert(ci);}

void test8 () {
    // read-only reference
    // immutable int
    int        i  = 2;
    const int  ci = 3;
//  const int& rc;      // error: 'rc' declared as reference but not initialized
    const int& rc = ci;
    const int& sc = i;
//  ++sc;               // error: increment of read-only reference 'sc'
    assert(rc);
    assert(sc);}





T*
const T*
T* const       ~ T&
const T* const ~ const T&
