// -----------
// Mon,  3 Feb
// -----------

/*
Calendar
    https://apps.cs.utexas.edu/calendar/

Reminders
    Sun, 9 Feb: Blog  #4
    Sun, 9 Feb: Paper #4

    please, NO phones, NO tablets, NO laptops
    I'll publish these notes daily on GitLab

Events
    Mon, 3 Feb, 6 pm, Dobie food court
    UTCC: UT Chess Club meeting
    https://sites.utexas.edu/utchessclub/

    Fri,  7 Feb, 2 pm, GDC 1.304
    CS 104c: Competitive Programming
    https://www.cs.utexas.edu/~downing/cs104c/

    Wed, 12 Feb: 5 pm, GDC auditorium
    UTPC: UT Programming Contest
    Contest #2
    https://www.cs.utexas.edu/~utpc/
*/





/*
last time
    consts

this time
    iteration
*/





// -------------
// Iteration.cpp
// -------------

void test1 () {
    int i = 0;
    int s = 0;
    while (i != 10) {
        s += i;
        ++i;}
    assert(i == 10);
    assert(s == 45);}

void test2 () {
    int i = 0;
    int s = 0;
    do {
        s += i;
        ++i;}
    while (i != 10);
    assert(i == 10);
    assert(s == 45);}

void test3 () {
    int s = 0;
    for (int i = 0; i != 10; ++i)
        s += i;
//  assert(i == 10);              // error: name lookup of 'i' changed for new ISO 'for' scoping
    assert(s == 45);}

void test4 () {
    const int a[] = {2, 3, 4};
    const int b[] = {5, 6, 7};
    int s = 0;
    for (int i = 0, j = 0; i != 3; ++i, ++j)
        s += a[i] + b[j];
    assert(s == 27);}

void test5 () {
    int a[] = {2, 3, 4};
    int s   = 0;
    for (int v : a)
        s += v;
    assert(s == 9);
    assert(equal(a, a + 3, begin({2, 3, 4})));}





int i = 0;

while (i != 3) {
    v = a[i];    // a must be indexable
    s += v;
    ++i;}





int* b = begin(a);
int* e = end(a);

while (b != e) {
    v = *b;      // a only needs to be iterable
    s += v;
    ++b;}





bool equal (int* b1, int* e1, int *b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;}
    return true;}

int a = {2, 3, 4};
int b = {2, 3, 4};

equal(a, a + 3, b);





int  a = {2, 3, 4};
long b = {2, 3, 4};

equal(a, a + 3, b);

bool equal (int* b1, int* e1, long *b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;}
    return true;}





equal (a + 5, a + 10, b + 15)
// smallest a could be is : 10
// smallest b could be is : 20





template <typename T1, typename T2>
bool equal (T1* b1, T1* e1, T2* b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;}
    return true;}

int  a = {2, 3, 4};
long b = {2, 3, 4};

equal (a, a + 3, b);
// T1 -> int
// T2 -> long





template <typename I1, typename I2>
bool equal (I1 b1, I1 e1, I2 b2) {
    while (b1 != e1) {
        if (*b1 != *b2)
            return false;
        ++b1;
        ++b2;}
    return true;}

set<int>   a = {2, 3, 4};
deque<int> b = {2, 3, 4}

equal(begin(a), end(a), begin(b));
// I1 -> set<int>::iterator
// I2 -> deque<int>::iterator





int  a = {2, 3, 4};
long b = {2, 3, 4};

equal(a, a + 3, b);
// I1 -> int*
// I2 -> long*
